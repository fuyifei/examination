package com.shinesend.base.service;

/**
 * @author sqh
 **/
public interface UpdateService<T, ID> {
    /**
     * 更新条目
     *
     * @param t 条目
     * @return
     */
    T update(T t);
}
