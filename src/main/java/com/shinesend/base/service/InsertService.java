package com.shinesend.base.service;

/**
 * @author sqh
 **/
public interface InsertService<T, ID> {

    /**
     * 保存
     *
     * @param t 条目
     * @return
     */
    T insert(T t);
}
