package com.shinesend.base.aspect;

import com.google.gson.Gson;
import com.shinesend.base.result.ApiResult;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * @author sqh
 **/
@Slf4j
@Aspect
@Component
public class ApiAspectHolder {

    /**
     * "execution(* com.grape.web.controller..*.*(..)) || execution(* com.grape.api.controller..*.*(..)) " +
     *             "|| execution(* com.grape.sys.controller..*.*(..)) || execution(* com.grape.open.controller..*.*(..))"
     */
    @Pointcut("execution(* com.shinesend.web.controller..*.*(..))")
    public void endpoint() {

    }

    @Before("endpoint()")
    public void doBeforeController(JoinPoint joinPoint) {
        Object[] obj = joinPoint.getArgs();
        Gson gson = new Gson();
        log.info("reqData:{}", reduce(gson.toJson(obj)));
    }

    @AfterReturning(pointcut = "endpoint()", returning = "retValue")
    public void doAfterController(JoinPoint joinPoint, Object retValue) {
        if (retValue instanceof ApiResult) {
            log.info("respData:{}", reduce(((ApiResult) retValue).toJson()));
        } else {
            log.info("respData:{}", retValue);
        }
    }

    public String reduce(String str) {
        str = str.replaceAll("fileBytes\":\".*?\"", "fileBytes\":\"...\"");
        str = str.replaceAll("fileBase64\":\".*?\"", "fileBase64\":\"...\"");
        return str;
    }

}
