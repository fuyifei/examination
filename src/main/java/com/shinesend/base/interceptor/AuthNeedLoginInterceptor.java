package com.shinesend.base.interceptor;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.shinesend.base.result.ApiCode;
import com.shinesend.base.result.ApiResult;
import com.shinesend.base.util.JwtUtil;
import com.shinesend.base.util.LoginUserInfo;
import com.shinesend.base.util.context.AccessContext;
import com.shinesend.web.service.admin.AdminFunctionRelService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;

/**
 * @author sqh
 **/
@Slf4j
@Component
public class AuthNeedLoginInterceptor implements HandlerInterceptor {
    @Value("${server.servlet.context-path}")
    private String contextPath;

    private static final String CONTENT_TYPE = "application/json;charset=utf-8";
    private static List<String> publicURIs = Arrays.asList(
            "/examination/web/adminInfo/save",
            "/examination/web/adminInfo/login",
            "/examination/miniApp/userInfo/login",
            "/examination/miniApp/fileInfo/getImg"
    );

    @Autowired
    private AdminFunctionRelService adminFunctionRelService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.info("check if the user is login in.");
        String reqUri = request.getRequestURI();
        log.info("reqUri:{}", reqUri);
        if (publicURIs.contains(reqUri)) {
            return true;
        }
        boolean ret = false;
        String token;
        if (request.getMethod().equalsIgnoreCase("get")) {
            token = request.getParameter("token");
            if (StringUtils.isBlank(token)) {
                token = request.getHeader("token");
            }
        } else {
            token = request.getHeader("token");
            // 检查 POST 请求的 ContentType
            String characterEncoding = request.getCharacterEncoding();
            if (characterEncoding == null || !characterEncoding.equalsIgnoreCase("utf-8")) {
                ApiResult result = new ApiResult(ApiCode.TOKEN_INVALID);
                response.getWriter().write(result.toJson());
                return false;
            }
        }
        boolean ret1 = false;
        String[] urlArr = reqUri.split("/");
        String systemType = urlArr[2];
        switch (systemType) {
            case "web":
                ret = webAuth(token);

                if (!ret) {
                    response.setContentType(CONTENT_TYPE);
                    response.getWriter().write(ApiResult.failure(ApiCode.TOKEN_INVALID).toJson());
                }
//                DecodedJWT verify = JwtUtil.verify(token);
//                Long adminId = verify.getClaim("adminId").asLong();
//                ret1 = adminFunctionRelService.checkBoot(adminId,"/"+urlArr[3]+"/"+urlArr[4]);
//                if(!ret1){
//                    response.setContentType(CONTENT_TYPE);
//                    response.getWriter().write(ApiResult.failure(ApiCode.USER_FUNC_NOT_EXISTS).toJson());
//                }
                break;
            case "miniApp":
                ret = miniappAuth(token);
                if (!ret) {
                    response.setContentType(CONTENT_TYPE);
                    response.getWriter().write(ApiResult.failure(ApiCode.TOKEN_INVALID).toJson());
                }
                break;
            case "open":
            case "api":
                ret = true;
                break;
            default:
                ret = false;
                response.setContentType(CONTENT_TYPE);
                response.getWriter().write(ApiResult.failure(ApiCode.TOKEN_INVALID).toJson());
        }
        return ret;
    }

    private boolean webAuth(String token) {
        DecodedJWT verify = JwtUtil.verify(token);
        Integer adminId = verify.getClaim("adminId").asInt();
        String loginName = verify.getClaim("loginName").asString();
        LoginUserInfo loginUserInfo = new LoginUserInfo();
        loginUserInfo.setAdminId(adminId);
        AccessContext.set(loginUserInfo);
        return adminId != null && StringUtils.isNotBlank(loginName);
    }

    private boolean miniappAuth(String token) {
        DecodedJWT verify = JwtUtil.verifySys(token);
        Integer userId = verify.getClaim("userId").asInt();
        String loginName = verify.getClaim("loginName").asString();
        LoginUserInfo loginUserInfo = new LoginUserInfo();
        loginUserInfo.setUserId(userId);
        AccessContext.set(loginUserInfo);
        return userId != null && StringUtils.isNotBlank(loginName);
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        AccessContext.remove();
    }
}
