package com.shinesend.base.exception;

/**
 * 自定义异常
 *
 * @author sqh
 **/
public class BaseException extends Exception {
    public BaseException(String message) {
        super(message);
    }

    public BaseException(String message, Throwable cause) {
        super(message, cause);
    }
}
