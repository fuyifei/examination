package com.shinesend.base.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;
import lombok.extern.slf4j.Slf4j;

/**
 * @author sqh
 */
@Slf4j
public class JwtUtil {
    private static String SECRET = "Shinesend,./$";

    public static String genToken(int adminId, String loginName) {
        Algorithm algorithm = Algorithm.HMAC256(SECRET);
        return JWT.create()
                .withIssuer("shinesend")
                .withClaim("adminId", adminId)
                .withClaim("loginName", loginName)
                .sign(algorithm);
    }

    public static DecodedJWT verify(String token) {
        Algorithm algorithm = Algorithm.HMAC256(SECRET);
        JWTVerifier verifier = JWT.require(algorithm)
                .withIssuer("shinesend")
                .build(); //Reusable verifier instance
        return verifier.verify(token);
    }

    public static String genSysToken(int sysUserId, String loginName) {
        Algorithm algorithm = Algorithm.HMAC256(SECRET);
        return JWT.create()
                .withIssuer("shinesend")
                .withClaim("userId", sysUserId)
                .withClaim("loginName", loginName)
                .sign(algorithm);
    }

    public static DecodedJWT verifySys(String token) {
        Algorithm algorithm = Algorithm.HMAC256(SECRET);
        JWTVerifier verifier = JWT.require(algorithm)
                .withIssuer("shinesend")
                .build(); //Reusable verifier instance
        return verifier.verify(token);
    }
}
