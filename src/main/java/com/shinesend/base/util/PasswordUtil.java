package com.shinesend.base.util;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * 密码工具类
 *
 * @author sqh
 **/
public class PasswordUtil {

    public static String randomSalt() {
        SecureRandom random = new SecureRandom();
        return Hex.encodeHexString(random.generateSeed(32));
    }

    public static String generatePassword(String password, String salt) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        return Base64.encodeBase64String(md.digest((password + salt).getBytes(StandardCharsets.UTF_8)));

    }

    public static boolean valid(String password, String salt, String passwordEnc) throws NoSuchAlgorithmException {
        return generatePassword(password, salt).equals(passwordEnc);
    }
}
