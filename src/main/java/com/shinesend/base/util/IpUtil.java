package com.shinesend.base.util;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @author sqh
 **/
@Slf4j
public class IpUtil {

    /**
     * 获取 IP 地址
     *
     * @param request 请求
     * @return ip地址
     */
    public static String getIpAddr(HttpServletRequest request) {
        String ipAddress = null;
        try {
            ipAddress = request.getHeader("x-forwarded-for");
            if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
                ipAddress = request.getHeader("Proxy-Client-IP");
            }
            if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
                ipAddress = request.getHeader("WL-Proxy-Client-IP");
            }
            if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
                ipAddress = request.getRemoteAddr();
                if ("127.0.0.1".equals(ipAddress)) {
                    // 根据网卡取本机配置的IP
                    InetAddress inet = null;
                    try {
                        inet = InetAddress.getLocalHost();
                    } catch (UnknownHostException e) {

                    }
                    ipAddress = inet.getHostAddress();
                }
            }
            // 对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割
            // "***.***.***.***".length()
            if (ipAddress != null && ipAddress.length() > 15) {
                // = 15
                if (ipAddress.indexOf(",") > 0) {
                    ipAddress = ipAddress.substring(0, ipAddress.indexOf(","));
                }
            }
        } catch (Exception e) {
            ipAddress = "";
        }
        return ipAddress;
    }

    /**
     * ip2region 获取位置
     * 使用 b-tree 算法查询
     *
     * @param ip
     * @return
     */
    // public static String getIpLocation(String ip) {
    //     String location = "";
    //     try {
    //         String path = IpUtil.class.getResource("/ip2region.db").getPath();
    //         File file = new File(path);
    //         if (!file.exists()) {
    //             return null;
    //         }
    //         DbConfig config = new DbConfig();
    //         DbSearcher search = new DbSearcher(config, path);
    //         DataBlock dataBlock = search.btreeSearch(ip);
    //         String[] region = dataBlock.getRegion().split("\\|");
    //         if (region.length > 0) {
    //             location = ("0".equals(region[0]) ? "" : region[0]) + ("0".equals(region[2]) ? "" : region[2])
    //                     + ("0".equals(region[3]) ? "" : region[3]);
    //         }
    //         //关闭文件流
    //         search.close();
    //     } catch (Exception e) {
    //         log.error("查询IP位置错误:{}", e.getMessage(), e);
    //     }
    //     return location;
    // }
}
