package com.shinesend.base.util.context;

import com.shinesend.base.util.LoginUserInfo;
import lombok.extern.slf4j.Slf4j;

/**
 * @author sqh
 **/
@Slf4j
public class AccessContext {
    private static final ThreadLocal<LoginUserInfo> THREAD_LOCAL = new ThreadLocal<>();

    public static LoginUserInfo get() {
        return THREAD_LOCAL.get();
    }

    public static void set(LoginUserInfo loginUserInfo) {
        THREAD_LOCAL.set(loginUserInfo);
    }

    public static void remove() {
        THREAD_LOCAL.remove();
    }
}
