package com.shinesend.base.util.updatelog;

import com.shinesend.base.service.SuperService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author sqh
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class TableUpdateLogService {

    private TableUpdateLogDao tableUpdateLogDao;

    public TableUpdateLogService(TableUpdateLogDao tableUpdateLogDao) {
        this.tableUpdateLogDao = tableUpdateLogDao;
    }


    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public TableUpdateLog save(TableUpdateLog tableUpdateLog) {
        return tableUpdateLogDao.save(tableUpdateLog);
    }
}
