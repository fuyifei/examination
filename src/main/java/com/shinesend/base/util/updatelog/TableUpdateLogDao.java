package com.shinesend.base.util.updatelog;

import com.shinesend.base.repository.BaseRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author sqh
 */
@Repository
public interface TableUpdateLogDao extends JpaRepository<TableUpdateLog, Long> {
}
