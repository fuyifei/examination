package com.shinesend.base.util.updatelog;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.Date;

/**
 * 修改日志
 *
 * @author sqh
 **/
@SuppressWarnings("all")
@Entity
@Table(name = "CRM_TABLE_UPDATE_LOG")
@Data
public class TableUpdateLog {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_CRM_TABLE_UPDATE")
    @SequenceGenerator(name = "SEQ_CRM_TABLE_UPDATE", sequenceName = "SEQ_CRM_TABLE_UPDATE", allocationSize = 1)
    private Long id;

    //表名
    @Column
    private String tableName;

    //列名
    @Column
    private String tableValue;

    //操作人
    @Column
    private Long userId;

    //操作人
    @Column
    private Long adminId;

    //修改类型(0:新增1:修改2:删除)
    @Column
    private int operatorType;

    //修改来源(0:web 1:miniApp)
    @Column
    private int fromType;

    /**
     * 元数据ID
     */
    private Long sourceId;

    //操作时间
    @CreationTimestamp
    @Column
    private Date modifyDate;
}
