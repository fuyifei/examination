package com.shinesend.base.util;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

import java.beans.PropertyDescriptor;
import java.util.HashSet;
import java.util.Set;

public class ObjectToObjectUtil {
    /**
     * 完全复制，不考虑空值
     *
     * @param source 源
     * @param target 目标
     */
    public static void objectCopy(Object source, Object target) {
        BeanUtils.copyProperties(source, target);
    }

    /**
     * 不完全复制，考虑空值
     *
     * @param source 源
     * @param target 目标
     */
    public static void objectCopyNotNull(Object source, Object target) {
        BeanUtils.copyProperties(source, target, getNullPropertyNames(source));
    }

    private static String[] getNullPropertyNames(Object source) {
        Set<String> emptyNames = new HashSet<>();
        final BeanWrapper src = new BeanWrapperImpl(source);
        PropertyDescriptor[] pdr = src.getPropertyDescriptors();
        for (PropertyDescriptor propertyDescriptor : pdr) {
            Object srcValue = src.getPropertyValue(propertyDescriptor.getName());
            if (srcValue == null) {
                emptyNames.add(propertyDescriptor.getName());
            }
        }
        String[] result = new String[emptyNames.size()];
        return emptyNames.toArray(result);
    }

}
