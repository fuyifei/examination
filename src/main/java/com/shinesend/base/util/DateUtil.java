package com.shinesend.base.util;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {
    /**
     * 获取某一天的开始时间
     *
     * @param date 时间
     * @return 开始时间
     */
    public static Date getBeginOfTheDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        calendar.set(year, month, day, 0, 0, 0);
        return calendar.getTime();
    }

    /**
     * 获取某一天的结束时间
     *
     * @param date 时间
     * @return 结束时间
     */
    public static Date getEndOfTheDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        calendar.set(year, month, day, 23, 59, 59);
        return calendar.getTime();
    }

    public static String dateToStr(Date date) {
        Instant instant = date.toInstant();
        LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return localDateTime.format(formatter);
    }

    public static String get14BitTimeStr() {
        Instant instant = new Date().toInstant();
        LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        return localDateTime.format(formatter);
    }


    /**
     * string 转 date
     *
     * @param string
     * @param template 时间格式 ex:yyyy-MM-dd HH:mm:ss
     */
    public static Date strToDate(String string, String template) {
        ZoneId zoneId = ZoneId.systemDefault();
        DateTimeFormatter df = DateTimeFormatter.ofPattern(template);
        LocalDateTime ldt = LocalDateTime.parse(string, df);
        return Date.from(ldt.atZone(zoneId).toInstant());
    }
}
