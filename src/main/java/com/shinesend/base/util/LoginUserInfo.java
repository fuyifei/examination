package com.shinesend.base.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author sqh
 **/
@Slf4j
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginUserInfo {
    private Integer userId;
    private Integer adminId;
}
