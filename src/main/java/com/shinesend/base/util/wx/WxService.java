package com.shinesend.base.util.wx;

import com.github.binarywang.wxpay.config.WxPayConfig;
import com.github.binarywang.wxpay.service.WxPayService;
import com.github.binarywang.wxpay.service.impl.WxPayServiceApacheHttpImpl;
import com.shinesend.base.config.ConfigService;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.impl.WxMpServiceHttpClientImpl;
import me.chanjar.weixin.mp.config.impl.WxMpDefaultConfigImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * 微信服务 Bean
 *
 * @author sqh
 */
@Component
public class WxService {
    @Autowired
    private ConfigService configService;

    /**
     * 微信支付 Service
     *
     * @return
     */
    @Bean
    public WxPayService getWxPayService() {
        WxPayService wxPayService = new WxPayServiceApacheHttpImpl();
        WxPayConfig wxPayConfig = new WxPayConfig();
        wxPayConfig.setAppId(configService.getConfigValue("wx.apiId"));
        wxPayConfig.setMchId(configService.getConfigValue("wx.mchid"));
        wxPayConfig.setMchKey(configService.getConfigValue("wx.mchKey"));
        wxPayConfig.setNotifyUrl(configService.getConfigValue("wx.notifyUrl"));
        wxPayService.setConfig(wxPayConfig);
        return wxPayService;
    }

    /**
     * 微信公众号 Service
     * @return
     */
    @Bean
    public WxMpService getWxMpService() {
        WxMpService wxMpService = new WxMpServiceHttpClientImpl();
        WxMpDefaultConfigImpl wxMpDefaultConfig = new WxMpDefaultConfigImpl();
        wxMpDefaultConfig.setAppId(configService.getConfigValue("wx.apiId"));
        wxMpDefaultConfig.setSecret(configService.getConfigValue("wx.apiSecret"));
        wxMpService.setWxMpConfigStorage(wxMpDefaultConfig);
        return wxMpService;
    }
}
