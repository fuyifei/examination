package com.shinesend.base.config;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.EncodedResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Properties;


/**
 * 读配置文件
 *
 * @author sqh
 **/
@Component("configService")
@Slf4j
public class ConfigService {
    private static final String CONFIG_FILE_NAME = "examination.config.file";
    private static final String CONFIG_FILE_PATH_DEFAULT = "config/exam.properties";

    @Autowired
    private Environment environment;
    private volatile Properties properties;


    public String getValue(String key, String defaultValue) {
        return environment.getProperty(key, defaultValue);
    }

    public String getConfigPath() {
        return getValue(CONFIG_FILE_NAME, CONFIG_FILE_PATH_DEFAULT);
    }

    private void initConfigEnv(String configFile) {
        try {
            if (properties == null) {
                EncodedResource encodedResource = new EncodedResource(new ClassPathResource(configFile), StandardCharsets.UTF_8);
                properties = PropertiesLoaderUtils.loadProperties(encodedResource);
            }
        } catch (IOException e) {
            log.error("读取配置文件错误，[{}}={}]，原因：{}", CONFIG_FILE_NAME, configFile, e.getMessage(), e);
        }
    }

    /**
     * 读取项目自定义配置函数
     *
     * @param key          key
     * @param defaultValue 默认值
     * @return 值
     */
    public String getConfigValue(String key, String defaultValue) {
        if (properties == null) {
            initConfigEnv(getConfigPath());
        }
        return properties.getProperty(key, defaultValue);
    }

    /**
     * 读取项目自定义配置函数
     *
     * @param key key
     * @return 值
     */
    public String getConfigValue(String key) {
        if (properties == null) {
            initConfigEnv(getConfigPath());
        }
        return properties.getProperty(key);
    }
}
