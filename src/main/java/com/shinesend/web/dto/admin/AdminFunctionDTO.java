package com.shinesend.web.dto.admin;

import lombok.Data;

import java.util.List;

@Data
public class AdminFunctionDTO {
    private Long adminId;
    private List<Long> ids;
    private Long parentId;
}
