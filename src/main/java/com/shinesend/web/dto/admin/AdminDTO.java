package com.shinesend.web.dto.admin;

import com.shinesend.base.service.query.Page;
import lombok.Data;

@Data
public class AdminDTO {
    private Long adminId;
    private String loginName;
    private String passwordHash;
    private String fullName;
    private String mobile;
    private Boolean useStatus;
    private String qwUserId;
    private Page page;
}
