package com.shinesend.web.dto.admin;

import lombok.Data;

@Data
public class LoginDTO {
    private String loginName;
    private String passwordHash;
}
