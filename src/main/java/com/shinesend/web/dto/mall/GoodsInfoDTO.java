package com.shinesend.web.dto.mall;

import com.shinesend.base.service.query.Page;
import lombok.Data;

@Data
public class GoodsInfoDTO {

    private Long id;
    private String goodsName;
    private String saPrice;
    private String disPrice;
    private String describe;
    private Long goodsImg;
    private String imgBase64;
    private String imgType;
    private Page page;
}
