package com.shinesend.web.dto.question;

import lombok.Data;

@Data
public class AnswerDTO {
    private Long answerId;
    // 题目ID
    private Long questionId;
    //答案内容
    private String answerContent;
    //正确标识
    private Integer correctFlag;
}
