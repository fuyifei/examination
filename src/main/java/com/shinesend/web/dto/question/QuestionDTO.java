package com.shinesend.web.dto.question;

import com.shinesend.base.service.query.Page;
import lombok.Data;

@Data
public class QuestionDTO {

    private Long questionId;
    //题目
    private String questionStem;
    //试题类别（0国考；1省考）
    private Long sortId;
    //解析
    private String questionAnalysis;

    private Page page;
}
