package com.shinesend.web.dto.systemconfig;

import com.shinesend.base.service.query.Page;
import lombok.Data;

import java.util.Date;
@Data
public class MSMInfoDTO {
    private Long smsPlatformId;
    private Long smsPlatform;
    private String smsUserId;
    private String smsPassword;
    private String smsKey;
    private String certPath;
    private Long useStatus;
    private Date creDate;
    private Long inputManId;
    private Date moDiDate;
    private Long moDiManId;
    private Date confirmDate;
    private Long confirmManId;
    private Date invalidDate;
    private Long invalidManId;
    private String memo;
    private String url;
    private Long spaceTime;
    private String signature;

    private String smsPlatformName;
    private Integer signPosition;
    private Page page;
}
