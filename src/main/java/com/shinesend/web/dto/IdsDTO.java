package com.shinesend.web.dto;

import lombok.Data;

import java.util.List;

@Data
public class IdsDTO {
    private List<Long> ids;

    private String flagName;

    private Integer flag;
}
