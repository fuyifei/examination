package com.shinesend.web.entity.admin;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author sqh
 **/
@Entity
@Table(name = "CRM_FUNCTION_INFO")
@Data
public class FunctionInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_CRM_FUNCTION_INFO")
    @SequenceGenerator(name = "SEQ_CRM_FUNCTION_INFO", sequenceName = "SEQ_CRM_FUNCTION_INFO", allocationSize = 1)
    private Long functionId;
    @Column
    private String functionName;
    @Column
    private Long parentId;
    @Column
    private Integer functionTypeId;
    @Column
    private String functionUrl;
    @Column
    private Integer functionOrder;
    @Column(name = "FUNCTION_E_NAME")
    private String functionEName;
    @Column
    private String functionIcon;

}
