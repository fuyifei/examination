package com.shinesend.web.entity.admin;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author sqh
 **/
@Entity
@Table(name = "ADMIN_INFO")
@Data
public class AdminInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ADMIN_INFO")
    @SequenceGenerator(name = "SEQ_ADMIN_INFO", sequenceName = "SEQ_ADMIN_INFO", allocationSize = 1)
    private Long adminId;
    @Column
    private String loginName;

    @Column
    @JsonIgnore
    private String passwordHash;
    @Column
    @JsonIgnore
    private String salt;
//    @Column
//    private String shopId;

    @Column
    private String fullName;
    @Column
    private String mobile;
    /**
     * true 删除
     * false 正常
     */
    @Column
    private Boolean useStatus;

    @Column
    private Integer adminType;
    @Column
    private String qwUserId;

    @Column
    @CreationTimestamp
    private Date dataCreated;
    @Column
    @UpdateTimestamp
    private Date lastUpdated;
}
