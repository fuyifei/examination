package com.shinesend.web.entity.admin;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author sqh
 **/
@Entity
@Table(name = "CRM_ADMIN_FUNCTION_REL")
@Data
public class AdminFunctionRel {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_CRM_ADMIN_FUNCTION_REL")
    @SequenceGenerator(name = "SEQ_CRM_ADMIN_FUNCTION_REL", sequenceName = "SEQ_CRM_ADMIN_FUNCTION_REL", allocationSize = 1)
    private Long id;
    @Column
    private Long adminId;
    @Column
    private Long functionId;
    @Column
    private Long inputManId;
    @Column
    @CreationTimestamp
    private Date dataCreated;
}
