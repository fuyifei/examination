package com.shinesend.web.entity.question;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.Date;


@Entity
@Table(name = "ANSWER")
@Data
public class Answer {


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ANSWER")
    @SequenceGenerator(name = "SEQ_ANSWER", sequenceName = "SEQ_ANSWER", allocationSize = 1)
    @Column(name = "ANSWER_ID")
    private Long answerId;
    // 题目ID
    @Column(name = "QUESTION_ID")
    private Long questionId;
    //答案内容
    @Column(name = "ANSWER_CONTENT")
    private String answerContent;
    //正确标识
    @Column(name = "CORRECT_FLAG")
    private Integer correctFlag;

    @Column
    private Long inputManId;
    @Column
    private Long updateManId;

    @CreationTimestamp
    @Column(name = "DATA_CREATED")
    private Date dataCreated;

    @UpdateTimestamp
    @Column(name = "LAST_UPDATED")
    private Date lastUpdated;


}
