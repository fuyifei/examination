package com.shinesend.web.entity.question;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "EXAM_LOG")
@Data
public class ExamLog {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_EXAM_LOG")
    @SequenceGenerator(name = "SEQ_EXAM_LOG", sequenceName = "SEQ_EXAM_LOG", allocationSize = 1)
    @Column
    private Long id;

    @Column
    private Long userId;
    @Column
    private Long sortId;

    @Column
    private Double score;
    @CreationTimestamp
    @Column
    private Date dataCreated;
    @UpdateTimestamp
    @Column
    private Date lastUpdated;
}
