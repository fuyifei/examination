package com.shinesend.web.entity.question;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.Date;


@Entity
@Table(name = "QUESTION")
@Data
public class Question {


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_QUESTION")
    @SequenceGenerator(name = "SEQ_QUESTION", sequenceName = "SEQ_QUESTION", allocationSize = 1)
    @Column(name = "QUESTION_ID")
    private Long questionId;
    //题目
    @Column(name = "QUESTION_STEM")
    private String questionStem;
    //试题类别（0国考；1省考）
    @Column(name = "SORT_ID")
    private Long sortId;
    //解析
    @Column(name = "QUESTION_ANALYSIS")
    private String questionAnalysis;

    @Column
    private Long inputManId;
    @Column
    private Long updateManId;
    @CreationTimestamp
    @Column(name = "DATA_CREATED")
    private Date dataCreated;

    @UpdateTimestamp
    @Column(name = "LAST_UPDATED")
    private Date lastUpdated;


}
