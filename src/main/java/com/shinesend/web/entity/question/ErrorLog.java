package com.shinesend.web.entity.question;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "ERROR_LOG")
@Data
public class ErrorLog {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ERROR_LOG")
    @SequenceGenerator(name = "SEQ_ERROR_LOG", sequenceName = "SEQ_ERROR_LOG", allocationSize = 1)
    @Column
    private Long id;

    @Column
    private Long userId;

    @Column
    private Long questionId;

    @Column
    private Integer resultStatus;


    @Column
    @CreationTimestamp
    private Date dataCreated;


    @Column
    @UpdateTimestamp
    private Date lastUpdated;

}
