package com.shinesend.web.entity.mall;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "goods_info")
@Data
public class GoodsInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_GOODS_INFO")
    @SequenceGenerator(name = "SEQ_GOODS_INFO", sequenceName = "SEQ_GOODS_INFO", allocationSize = 1)
    @Column
    private Long id;
    @Column
    private String goodsName;
    @Column
    private String saPrice;
    @Column
    private String disPrice;
    @Column
    private String describe;
    @Column
    private Long goodsImg;
    @CreationTimestamp
    @Column(name = "DATA_CREATED")
    private Date dataCreated;

    @UpdateTimestamp
    @Column(name = "LAST_UPDATED")
    private Date lastUpdated;

    @Column
    private Long inputManId;
    @Column
    private Long updateManId;


}
