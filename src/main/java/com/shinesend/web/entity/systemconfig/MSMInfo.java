package com.shinesend.web.entity.systemconfig;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name="SYS_SMSPLATFORM_DEF")
@Data
public class MSMInfo {
    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="SEQ_SYS_SMSPLATFORM_DEF")
    @SequenceGenerator(name="SEQ_SYS_SMSPLATFORM_DEF", sequenceName="SEQ_SYS_SMSPLATFORM_DEF", allocationSize=1)
    @Column(name="SMSPLATFORMID")
    private Long smsPlatformId;
    @Column(name="SMSPLATFORM")
    private Long smsPlatform;
    @Column(name="SMSUESRID")
    private String smsUserId;
    @Column(name="SMSPASSWORD")
    private String smsPassword;
    @Column(name="SMSKEY")
    private String smsKey;
    @Column(name="CERTPATH")
    private String certPath;
    @Column(name="USESTATUS")
    private Long useStatus;
    @Column(name="CREDATE")
    @CreationTimestamp
    private Date creDate;
    @Column(name="INPUTMANID")
    private Long inputManId;
    @Column(name="MODIDATE")
    @UpdateTimestamp
    private Date moDiDate;
    @Column(name="MODIMANID")
    private Long moDiManId;
    @Column(name="CONFIRMDATE")
    private Date confirmDate;
    @Column(name="CONFIRMMANID")
    private Long confirmManId;
    @Column(name="INVALIDDATE")
    private Date invalidDate;
    @Column(name="INVALIDMANID")
    private Long invalidManId;
    @Column(name="MEMO")
    private String memo;
    @Column(name="URL")
    private String url;
    @Column(name="SPACETIME")
    private Long spaceTime;
    @Column(name="SIGNATURE")
    private String signature;
    @Column(name="SMSPLATFORMNAME")
    private String smsPlatformName;
    @Column(name="SIGNPOSITION")
    private Integer signPosition;
}
