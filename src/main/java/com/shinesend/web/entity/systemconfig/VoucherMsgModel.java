package com.shinesend.web.entity.systemconfig;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;

/**
 * 系统配置
 *
 * @author sqh
 **/
@Entity
@Table(name = "CRM_SYS_CONFIG")
@Data
public class VoucherMsgModel {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_CRM_SYS_CONFIG")
    @SequenceGenerator(name = "SEQ_CRM_SYS_CONFIG", sequenceName = "SEQ_CRM_SYS_CONFIG", allocationSize = 1)
    private Long id;
    @Column
    private Integer useStatus;
    @Column
    private String voucherMsgModel;
    @Column
    private Long inputManId;
    @Column
    private Long startManId;
    @Column
    private Long stopManId;
    @Column
    @CreationTimestamp
    private Date dataCreated;
    @Column
    private Date lastStartDate;
    @Column
    private Date lastStopDate;
    @Column
    private String wxMsgContent;
    @Column
    private Long wxMouldId;
    @Transient
    private String wxRealMouldId;
    @Transient
    private String wxMouldName;

}
