package com.shinesend.web.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

@Component
@Slf4j
public class ExportExcelUtil {

    public void export(String title, List<Map<String, String>> colNameList, List<Map<String, Object>> dataList, HttpServletResponse response) throws Exception {
        OutputStream out = new ByteArrayOutputStream();
//        HSSFWorkbook workbook = new HSSFWorkbook(); // 创建工作簿对象
        XSSFWorkbook workbook1 = new XSSFWorkbook();
        try {
            XSSFSheet sheet1 = workbook1.createSheet(title);
//            HSSFSheet sheet = workbook.createSheet(title); // 创建工作表
            XSSFRow rowm1 = sheet1.createRow(0);  // 产生表格标题行
//            HSSFRow rowm = sheet.createRow(0);  // 产生表格标题行
            XSSFCell cellTiltle1 = rowm1.createCell(0);   //创建表格标题列
//            HSSFCell cellTiltle = rowm.createCell(0);   //创建表格标题列
            XSSFCellStyle columnTopStyle1 = getXColumnTopStyle(workbook1);
//            HSSFCellStyle columnTopStyle = getColumnTopStyle(workbook);// 获取列头样式对象
            //合并表格标题行，合并列数为列名的长度,第一个0为起始行号，第二个1为终止行号，第三个0为起始列好，第四个参数为终止列号
            sheet1.addMergedRegion(new CellRangeAddress(0, 1, 0, (colNameList.size())));
//            sheet.addMergedRegion(new CellRangeAddress(0, 1, 0, (colNameList.size())));
            cellTiltle1.setCellStyle(columnTopStyle1);
//            cellTiltle.setCellStyle(columnTopStyle);    //设置标题行样式
            cellTiltle1.setCellValue(title);
//            cellTiltle.setCellValue(title);     //设置标题行值
            int columnNum = colNameList.size();     // 定义所需列数
//            HSSFRow rowRowName = sheet.createRow(2); // 在索引2的位置创建行(最顶端的行开始的第二行)
            XSSFRow rowRowName1 = sheet1.createRow(2);
            // 将列头设置到sheet的单元格中
            for (int n = -1; n < columnNum; n++) {
                XSSFCell cellRowName1 = rowRowName1.createCell(n + 1);
//                HSSFCell cellRowName = rowRowName.createCell(n + 1); // 创建列头对应个数的单元格
                if (n == -1) {
                    cellRowName1.setCellType(CellType.STRING);
//                    cellRowName.setCellType(CellType.STRING); // 设置列头单元格的数据类型
                    XSSFRichTextString text1 = new XSSFRichTextString("序号");
//                    HSSFRichTextString text = new HSSFRichTextString("序号");
                    cellRowName1.setCellValue(text1);
//                    cellRowName.setCellValue(text); // 设置列头单元格的值
                } else {
                    cellRowName1.setCellType(CellType.STRING);
//                    cellRowName.setCellType(CellType.STRING); // 设置列头单元格的数据类型
                    XSSFRichTextString text1 = new XSSFRichTextString(colNameList.get(n).get("colCnName"));
//                    HSSFRichTextString text = new HSSFRichTextString(colNameList.get(n).get("colCnName"));
                    cellRowName1.setCellValue(text1);
//                    cellRowName.setCellValue(text); // 设置列头单元格的值
                }
                cellRowName1.setCellStyle(columnTopStyle1);
//                cellRowName.setCellStyle(columnTopStyle); // 设置列头单元格样式
            }
            XSSFCellStyle style1 = getXStyle(workbook1);
//            HSSFCellStyle style = getStyle(workbook); // 获取单元格样式对象
            //    将查询出的数据设置到sheet对应的单元格中
            for (int i = 0; i < dataList.size(); i++) {
                Map rowMap = dataList.get(i);
                XSSFRow row1 = sheet1.createRow(i + 3);
//                HSSFRow row = sheet.createRow(i + 3);   // 创建所需的行数
                for (int j = -1; j < colNameList.size(); j++) {

                    XSSFCell cell1 = null;
//                    HSSFCell cell = null;   // 设置单元格的数据类型
                    if (j == -1) {
                        cell1 = row1.createCell(j + 1, CellType.NUMERIC);
//                        cell = row.createCell(j + 1, CellType.NUMERIC);
                        cell1.setCellValue(i + 1);
//                        cell.setCellValue(i + 1);

                    } else {
                        String colName = colNameList.get(j).get("colName");
                        String value = String.valueOf(rowMap.get(colName));
                        if (StringUtils.isNumeric(value)) {
                            cell1 = row1.createCell(j + 1, CellType.NUMERIC);
//                            cell = row.createCell(j + 1, CellType.NUMERIC);
                            cell1.setCellValue(value);
//                            cell.setCellValue(value);
                        } else {
                            cell1 = row1.createCell(j + 1, CellType.STRING);
//                            cell = row.createCell(j + 1, CellType.STRING);
                            if (StringUtils.isNotBlank(value)) {
                                XSSFRichTextString text1 = new XSSFRichTextString(value);
                                cell1.setCellValue(text1);
                                HSSFRichTextString text = new HSSFRichTextString(value);
//                                cell.setCellValue(text); // 设置单元格的值
                            }
                        }

                    }
                    cell1.setCellStyle(style1);
//                    cell.setCellStyle(style); // 设置单元格样式
                }
            }
/***************************************************
            //    让列宽随着导出的列长自动适应
            for (int colNum = 0; colNum < columnNum; colNum++) {
                int columnWidth = sheet.getColumnWidth(colNum) / 256;
                for (int rowNum = 0; rowNum < sheet.getLastRowNum(); rowNum++) {
                    HSSFRow currentRow;
                    // 当前行未被使用过
                    if (sheet.getRow(rowNum) == null) {
                        currentRow = sheet.createRow(rowNum);
                    } else {
                        currentRow = sheet.getRow(rowNum);
                    }
                    if (currentRow.getCell(colNum) != null) {
                        HSSFCell currentCell = currentRow.getCell(colNum);
                        if (currentCell.getCellType() == CellType.STRING) {
                            int length = currentCell.getStringCellValue()
                                    .getBytes().length;
                            if (columnWidth < length) {
                                columnWidth = length;
                            }
                        }

                    }
                }
                if (colNum == 0) {
                    sheet.setColumnWidth(colNum, (columnWidth - 2) * 256);
                } else {
                    sheet.setColumnWidth(colNum, (columnWidth + 4) * 256);
                }
            }
 **************************************************************/
            out = response.getOutputStream();

            response.setHeader("content-disposition", "attachment;filename="
                    + URLEncoder.encode(title + ".xlsx", "UTF-8"));
            workbook1.write(out);

//            response.setHeader("content-disposition", "attachment;filename="
//                    + URLEncoder.encode(title + ".xls", "UTF-8"));
//            workbook.write(out);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
//            workbook.close();
            workbook1.close();
            if (out != null){
                out.close();
            }


        }

    }

    /*
     * 列头单元格样式
     */
    private HSSFCellStyle getColumnTopStyle(HSSFWorkbook workbook) {

        // 设置字体
        HSSFFont font = workbook.createFont();
        // 设置字体大小
        font.setFontHeightInPoints((short) 11);
        // 字体加粗
        font.setBold(true);
        // 设置字体名字
        font.setFontName("Courier New");
        // 设置样式;
        HSSFCellStyle style = workbook.createCellStyle();
        // 设置底边框;
        style.setBorderBottom(BorderStyle.THIN);
        // 设置底边框颜色;
        style.setBottomBorderColor(HSSFColor.HSSFColorPredefined.BLACK.getIndex());
        // 设置左边框;
        style.setBorderLeft(BorderStyle.THIN);
        // 设置左边框颜色;
        style.setLeftBorderColor(HSSFColor.HSSFColorPredefined.BLACK.getIndex());
        // 设置右边框;
        style.setBorderRight(BorderStyle.THIN);
        // 设置右边框颜色;
        style.setRightBorderColor(HSSFColor.HSSFColorPredefined.BLACK.getIndex());
        // 设置顶边框;
        style.setBorderTop(BorderStyle.THIN);
        // 设置顶边框颜色;
        style.setTopBorderColor(HSSFColor.HSSFColorPredefined.BLACK.getIndex());
        // 在样式用应用设置的字体;
        style.setFont(font);
        // 设置自动换行;
        style.setWrapText(false);
        // 设置水平对齐的样式为居中对齐;
        style.setAlignment(HorizontalAlignment.CENTER);
        // 设置垂直对齐的样式为居中对齐;
        style.setVerticalAlignment(VerticalAlignment.CENTER);

        return style;

    }
    private XSSFCellStyle getXColumnTopStyle(XSSFWorkbook workbook){
        // 设置字体
        XSSFFont font = workbook.createFont();
        // 设置字体大小
        font.setFontHeightInPoints((short) 11);
        // 字体加粗
        font.setBold(true);
        // 设置字体名字
        font.setFontName("Courier New");
        // 设置样式;
        XSSFCellStyle style = workbook.createCellStyle();
        // 设置底边框;
        style.setBorderBottom(BorderStyle.THIN);
        // 设置底边框颜色;
        style.setBottomBorderColor(HSSFColor.HSSFColorPredefined.BLACK.getIndex());
        // 设置左边框;
        style.setBorderLeft(BorderStyle.THIN);
        // 设置左边框颜色;
        style.setLeftBorderColor(HSSFColor.HSSFColorPredefined.BLACK.getIndex());
        // 设置右边框;
        style.setBorderRight(BorderStyle.THIN);
        // 设置右边框颜色;
        style.setRightBorderColor(HSSFColor.HSSFColorPredefined.BLACK.getIndex());
        // 设置顶边框;
        style.setBorderTop(BorderStyle.THIN);
        // 设置顶边框颜色;
        style.setTopBorderColor(HSSFColor.HSSFColorPredefined.BLACK.getIndex());
        // 在样式用应用设置的字体;
        style.setFont(font);
        // 设置自动换行;
        style.setWrapText(false);
        // 设置水平对齐的样式为居中对齐;
        style.setAlignment(HorizontalAlignment.CENTER);
        // 设置垂直对齐的样式为居中对齐;
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        return style;
    }
    /*
     * 列数据信息单元格样式
     */
    private HSSFCellStyle getStyle(HSSFWorkbook workbook) {
        // 设置字体
        HSSFFont font = workbook.createFont();
        // 设置字体大小
        // font.setFontHeightInPoints((short)10);
        // 字体加粗
        // font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        // 设置字体名字
        font.setFontName("Courier New");
        // 设置样式;
        HSSFCellStyle style = workbook.createCellStyle();
        // 设置底边框;
        style.setBorderBottom(BorderStyle.THIN);
        // 设置底边框颜色;
        style.setBottomBorderColor(HSSFColor.HSSFColorPredefined.BLACK.getIndex());
        // 设置左边框;
        style.setBorderLeft(BorderStyle.THIN);
        // 设置左边框颜色;
        style.setLeftBorderColor(HSSFColor.HSSFColorPredefined.BLACK.getIndex());
        // 设置右边框;
        style.setBorderRight(BorderStyle.THIN);
        // 设置右边框颜色;
        style.setRightBorderColor(HSSFColor.HSSFColorPredefined.BLACK.getIndex());
        // 设置顶边框;
        style.setBorderTop(BorderStyle.THIN);
        // 设置顶边框颜色;
        style.setTopBorderColor(HSSFColor.HSSFColorPredefined.BLACK.getIndex());
        // 在样式用应用设置的字体;
        style.setFont(font);
        // 设置自动换行;
        style.setWrapText(false);
        // 设置水平对齐的样式为居中对齐;
        style.setAlignment(HorizontalAlignment.CENTER);
        // 设置垂直对齐的样式为居中对齐;
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        return style;
    }

    private XSSFCellStyle getXStyle(XSSFWorkbook workbook) {
        // 设置字体
        XSSFFont font = workbook.createFont();
        // 设置字体大小
        // font.setFontHeightInPoints((short)10);
        // 字体加粗
        // font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        // 设置字体名字
        font.setFontName("Courier New");
        // 设置样式;
        XSSFCellStyle style = workbook.createCellStyle();
        // 设置底边框;
        style.setBorderBottom(BorderStyle.THIN);
        // 设置底边框颜色;
        style.setBottomBorderColor(HSSFColor.HSSFColorPredefined.BLACK.getIndex());
        // 设置左边框;
        style.setBorderLeft(BorderStyle.THIN);
        // 设置左边框颜色;
        style.setLeftBorderColor(HSSFColor.HSSFColorPredefined.BLACK.getIndex());
        // 设置右边框;
        style.setBorderRight(BorderStyle.THIN);
        // 设置右边框颜色;
        style.setRightBorderColor(HSSFColor.HSSFColorPredefined.BLACK.getIndex());
        // 设置顶边框;
        style.setBorderTop(BorderStyle.THIN);
        // 设置顶边框颜色;
        style.setTopBorderColor(HSSFColor.HSSFColorPredefined.BLACK.getIndex());
        // 在样式用应用设置的字体;
        style.setFont(font);
        // 设置自动换行;
        style.setWrapText(false);
        // 设置水平对齐的样式为居中对齐;
        style.setAlignment(HorizontalAlignment.CENTER);
        // 设置垂直对齐的样式为居中对齐;
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        return style;
    }

}
