package com.shinesend.web.util;

public class QueryPageUtil {

    public static StringBuilder getQueryPageSql(StringBuilder sqlSb, Integer pageSize, Integer currentNum) {
        int start = (currentNum - 1) * pageSize;
        int end = start + pageSize;
        StringBuilder queryPageSqlSb = new StringBuilder();
        queryPageSqlSb.append("select * from (");
        queryPageSqlSb.append(" select temp.*, rowNum AS rowNo from (");
        queryPageSqlSb.append(sqlSb);
        queryPageSqlSb.append(" ) temp ");
        queryPageSqlSb.append(" where rowNum <= " + end + ") temp1 ");
        queryPageSqlSb.append(" where temp1.rowNo > " + start);
        return queryPageSqlSb;
    }

}
