package com.shinesend.web.util;

import com.shinesend.base.config.ConfigService;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Slf4j
public class QiYeWeiXinUtil {
    @Autowired
    private ConfigService configService;

    public String getAccessToken() {
        String appId = configService.getConfigValue("qy.wx.apiId");
        String appSecret = configService.getConfigValue("qy.wx.appSecret");
        String url = "https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid="
                + appId + "&corpsecret=" + appSecret;
        String resp = sendGet(url);
        String accessToken = "";
        JSONObject resultJson = new JSONObject(resp);
        if ("0".equals(String.valueOf(resultJson.get("errcode")))) {
            accessToken = String.valueOf(resultJson.get("access_token"));
        } else {
            accessToken = String.valueOf(resultJson.get("errmsg"));
        }
        return accessToken;
    }

    private String sendGet(String url) {


        String result = "";
        BufferedReader in = null;
        try {
            URL realUrl = new URL(url);
            // 打开和URL之间的连接
            URLConnection connection = realUrl.openConnection();
            // 设置通用的请求属性
            connection.setRequestProperty("accept", "*/*");
            connection.setRequestProperty("connection", "Keep-Alive");
            connection.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");

            // 建立实际的连接
            connection.connect();
            // 获取所有响应头字段
            Map<String, List<String>> map = connection.getHeaderFields();
            // 遍历所有的响应头字段
            for (String key : map.keySet()) {
                System.out.println(key + "--->" + map.get(key));
            }
            // 定义 BufferedReader输入流来读取URL的响应
            in = new BufferedReader(new InputStreamReader(connection.getInputStream(), "utf-8"));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
            System.out.println("发送GET请求出现异常！" + e);
            e.printStackTrace();
        }

        // 使用finally块来关闭输入流

        finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return result;
    }

    public String sendPost(Integer type, String accessToken, String param) {

        PrintWriter out = null;
        BufferedReader in = null;
        String result = "";

        String url = "https://qyapi.weixin.qq.com/cgi-bin/externalcontact";
        if (type == 0) {
            //查询标签
            url += "/get_corp_tag_list?access_token=" + accessToken;

        } else if (type == 1) {
            //新增标签
            url += "/add_corp_tag?access_token=" + accessToken;

        } else if (type == 2) {
            //修改标签
            url += "/edit_corp_tag?access_token=" + accessToken;
        } else if (type == 3) {
            //删除标签
            url += "/del_corp_tag?access_token=" + accessToken;
        } else if (type == 4) {
            //获取外部人员ID
            url += "/unionid_to_external_userid?access_token=" + accessToken;
        } else if (type == 5) {
            // 贴标签
            url += "/mark_tag?access_token=" + accessToken;
        } else if (type == 6) {
            //批量获取 客户
            url = "https://qyapi.weixin.qq.com/cgi-bin/externalcontact/batch/get_by_user?access_token=" + accessToken;
        } else if (type == 7) {
            url = "https://qyapi.weixin.qq.com/cgi-bin/service/get_provider_token";
        } else if (type == 8) {
            //群发消息
            url += "/add_msg_template?access_token=" +accessToken;
        }

        try {
            URL realUrl = new URL(url);

            // 打开和URL之间的连接
            URLConnection conn = realUrl.openConnection();

            // 设置通用的请求属性
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");

            // 发送POST请求必须设置如下两行
            conn.setDoOutput(true);
            conn.setDoInput(true);
            // 获取URLConnection对象对应的输出流
            out = new PrintWriter(new OutputStreamWriter(conn.getOutputStream(), "utf-8"));
            // 发送请求参数
            out.print(param);
            // flush输出流的缓冲
            out.flush();
            // 定义BufferedReader输入流来读取URL的响应
            in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
            e.printStackTrace();
//            log.error("发送 POST 请求出现异常！" + e.getMessage(), e);
        } finally {
            // 使用finally块来关闭输出流、输入流
            try {
                if (out != null) {
                    out.close();
                }
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }

        }
        return result;
    }

    public String getUnionId(String wxOpenId) {
        String accessToken = getGzhAccessToken();
        String url = "https://api.weixin.qq.com/cgi-bin/user/info?access_token="
                + accessToken + "&openid=" + wxOpenId + "&lang=zh_CN";
        String result = sendGet(url);
        JSONObject resJson = new JSONObject(result);
        String unionId = "";
        if(resJson.has("subscribe")){
            if (resJson.getInt("subscribe") == 1) {
                unionId = resJson.getString("unionid");
            }
        }

        return unionId;
    }

    public List<Map<String, Object>> queryDeptList() {
        String url = "https://qyapi.weixin.qq.com/cgi-bin/department/list?access_token=" + getAccessToken();
        String result = sendGet(url);
        JSONObject reJson = new JSONObject(result);
        List<Map<String, Object>> mapList = new ArrayList<>();
        if (reJson.getInt("errcode") == 0) {
            JSONArray departmentArr = reJson.getJSONArray("department");
            for (int i = 0; i < departmentArr.length(); i++) {
                JSONObject dept = departmentArr.getJSONObject(i);
                Map map = new HashMap();
                map.put("id", dept.getInt("id"));
                map.put("name", dept.getString("name"));
                map.put("parentId", dept.getInt("parentid"));
                mapList.add(map);
            }
        }
        return mapList;
    }

    public List<Map<String, Object>> queryWqUser(Long deptId) {
        String url = "https://qyapi.weixin.qq.com/cgi-bin/user/simplelist?access_token="
                + getAccessToken() + "&department_id=" + deptId;
        String result = sendGet(url);
        JSONObject reJson = new JSONObject(result);
        List<Map<String, Object>> mapList = new ArrayList<>();
        if (reJson.getInt("errcode") == 0) {
            JSONArray userArr = reJson.getJSONArray("userlist");
            for (int i = 0; i < userArr.length(); i++) {
                JSONObject user = userArr.getJSONObject(i);
                Map map = new HashMap();
                map.put("userId", user.getString("userid"));
                map.put("name", user.getString("name"));
                mapList.add(map);
            }
        }
        return mapList;
    }

    public String getGzhAccessToken() {
        String appId = configService.getConfigValue("wx.gzh.appId");
        String appSecret = configService.getConfigValue("wx.gzh.appSecret");
        String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="
                + appId + "&secret=" + appSecret;
        String resp = sendGet(url);
        String accessToken = "";
        JSONObject resultJson = new JSONObject(resp);
        if (resultJson.has("access_token")) {
            accessToken = resultJson.getString("access_token");
        }
        return accessToken;
    }

    public String getFirstAccessToken() {
        String appId = configService.getConfigValue("qy.wx.apiId");
        String appFirstSecret = configService.getConfigValue("qy.wx.firstSecret");
        String url = "https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid=" + appId + "&corpsecret=" + appFirstSecret;
        String resp = sendGet(url);
        String accessToken = "";
        JSONObject resultJson = new JSONObject(resp);
        if (resultJson.has("access_token")) {
            accessToken = resultJson.getString("access_token");
        }
        return accessToken;
    }
//    public String getProviderAccessToken() {
//        String appId = configService.getConfigValue("qy.wx.apiId");
//        String appProviderSecret = configService.getConfigValue("qy.wx.providerSecret");
//        String url = "https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid=" + appId + "&corpsecret=" + appProviderSecret;
//        String resp = sendGet(url);
//        String accessToken = "";
//        JSONObject resultJson = new JSONObject(resp);
//        if (resultJson.has("access_token")) {
//            accessToken = resultJson.getString("access_token");
//        }
//        return accessToken;
//    }

    public String getMinAppAccessToken() {
        String appId = configService.getConfigValue("qy.wx.apiId");
        String appSecret = configService.getConfigValue("wx.qy.secret");
        String url = "https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid=" + appId + "&corpsecret=" + appSecret;
        String resp = sendGet(url);
        String accessToken = "";
        JSONObject resultJson = new JSONObject(resp);
        if (resultJson.has("access_token")) {
            accessToken = resultJson.getString("access_token");
        }
        return accessToken;
    }

    public Map<String, String> qyLogin(String code) {

        String accessToken = getMinAppAccessToken();
        String url = "https://qyapi.weixin.qq.com/cgi-bin/miniprogram/jscode2session?access_token=" + accessToken + "&js_code=" + code + "&grant_type=authorization_code";
        String resp = sendGet(url);
        JSONObject resultJson = new JSONObject(resp);
        String  userId = resultJson.getString("userid");
        String  sessionKey = resultJson.getString("session_key");
        Map<String, String> map = new HashMap<>();
//        userId = "FuYiFei";
        map.put("userId", userId);
        map.put("sessionKey", sessionKey);
        return map;
    }

}
