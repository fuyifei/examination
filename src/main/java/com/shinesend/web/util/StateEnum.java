package com.shinesend.web.util;

/**
 * 会员群查询 状态  1执行中， 2执行结束，3执行失败
 */
public enum StateEnum {

    RUNING(1,"执行中"),
    RUNING_END(2,"执行结束"),
    RUNING_FAIL(3,"执行失败");

    private Integer code;
    private String value;

    private StateEnum(Integer code,String value) {
        this.code = code;
        this.value = value;
    }

    public Integer getCode() {
        return code;
    }


    public void setCode(Integer code) {
        this.code = code;
    }


    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return String.valueOf(this.value);
    }

    public static String getValueByCode(int code) {
        StateEnum[] values = StateEnum.values();

        for (StateEnum value : values) {
            if (code == value.getCode()) {
                return value.getValue();
            }
        }
        return "";
    }
}
