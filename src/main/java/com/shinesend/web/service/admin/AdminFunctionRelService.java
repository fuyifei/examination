package com.shinesend.web.service.admin;

import com.shinesend.base.service.SuperService;
import com.shinesend.web.dao.admin.AdminFunctionRelDao;
import com.shinesend.web.entity.admin.AdminFunctionRel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AdminFunctionRelService extends SuperService<AdminFunctionRel, Long, AdminFunctionRelDao> {
    @Autowired
    private AdminFunctionRelDao adminFunctionRelDao;

    public List<AdminFunctionRel> findAllByAdminId(Long adminId) {
        return adminFunctionRelDao.findAllByAdminIdOrderByFunctionId(adminId);
    }
    public List<Map<String, Object>> findAllByAdminIdAndParentId(Long adminId,Long parentId){
        StringBuilder sqlSb = new StringBuilder();

        sqlSb.append(" select a.function_id,a.function_name,a.parent_id,a.function_type_id,a.function_url,a.function_e_name,a.function_icon ");
        sqlSb.append(" from crm_function_info a ,crm_admin_function_rel b ");
        sqlSb.append(" where a.function_id = b.function_id ");
        sqlSb.append(" and b.admin_id = "+ adminId );
        sqlSb.append(" and a.parent_id = "+ parentId);
        sqlSb.append(" order by a.function_order asc ");
        List<Object[]> objList = adminFunctionRelDao.queryObjBySql(sqlSb.toString());
        List<Map<String, Object>> respList = new ArrayList<>();
        objList.forEach(objects1 -> {
            Map<String, Object> map = new HashMap<>();
            map.put("functionId", objects1[0]);
            map.put("functionName", objects1[1]);
            map.put("parentId", objects1[2]);
            map.put("functionTypeId", objects1[3]);
            map.put("name",objects1[5]);
            map.put("path",objects1[4]);
            map.put("icon",objects1[6]);
            respList.add(map);
        });
        return respList;
    }

    public List<Map<String,Object>> findAllByAdminIdAndParentIdAll(Long adminId,Long parentId){
        StringBuilder sqlSb = new StringBuilder(" select a.function_id,a.function_name,a.parent_id,a.function_type_id,a.function_url,a.function_e_name,a.function_icon from crm_function_info a where a.parent_id ="+parentId);
        List<Object[]> objList = adminFunctionRelDao.queryObjBySql(sqlSb.toString());
        sqlSb = new StringBuilder();
        sqlSb.append(" select a.function_id,a.function_name,a.parent_id,a.function_type_id,a.function_url,a.function_e_name,a.function_icon ");
        sqlSb.append(" from crm_function_info a ,crm_admin_function_rel b ");
        sqlSb.append(" where a.function_id = b.function_id ");
        sqlSb.append(" and b.admin_id = "+ adminId );
        sqlSb.append(" and a.parent_id = "+ parentId);
        List<Object[]> objList1 = adminFunctionRelDao.queryObjBySql(sqlSb.toString());
        List<Map<String, Object>> respList = new ArrayList<>();
        for(int i = 0 ; i <objList.size();i++){
            Object[] objects1 = objList.get(i);
            Map<String,Object> menu = new HashMap<>();
            menu.put("functionId", objects1[0]);
            menu.put("functionName", objects1[1]);
            menu.put("parentId", objects1[2]);
            menu.put("functionTypeId", objects1[3]);
            menu.put("name",objects1[5]);
            menu.put("path",objects1[4]);
            menu.put("icon",objects1[6]);
            boolean bo = true;
            for(int j = 0 ;j<objList1.size();j++ ){
                if(objList.get(i)[0].equals(objList1.get(j)[0])){
                    bo = false;
                }
            }
            menu.put("root",bo);
            respList.add(menu);
        }

        return respList;
    }

    public Boolean checkBoot(Long adminId,String functionUrl){

        StringBuilder sqlSb = new StringBuilder();
        sqlSb.append(" select * from crm_function_info where function_url = '"+ functionUrl+"'" );
        List<Object[]> objList0 = adminFunctionRelDao.queryObjBySql(sqlSb.toString());
        if(objList0.size()==0){
            return true;
        }
        sqlSb = new StringBuilder();
        sqlSb.append(" select a.function_id,a.function_name,a.parent_id,a.function_type_id ");
        sqlSb.append(" from crm_function_info a ,crm_admin_function_rel b ");
        sqlSb.append(" where a.function_id = b.function_id ");
        sqlSb.append(" and b.admin_id = "+ adminId );
        sqlSb.append(" and a.function_url = '"+ functionUrl+"'");
        List<Object[]> objList = adminFunctionRelDao.queryObjBySql(sqlSb.toString());
        if(objList.size()>0){
            return true;
        }else{
            return false;
        }

    }
}
