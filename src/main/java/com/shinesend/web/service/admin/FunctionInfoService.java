package com.shinesend.web.service.admin;

import com.shinesend.web.dao.admin.AdminFunctionRelDao;
import com.shinesend.web.dao.admin.FunctionInfoDao;
import com.shinesend.web.entity.admin.AdminFunctionRel;
import com.shinesend.web.entity.admin.FunctionInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FunctionInfoService {
    @Autowired
    private FunctionInfoDao functionInfoDao;
    @Autowired
    private AdminFunctionRelDao adminFunctionRelDao;

    public FunctionInfo findById(Long functionId){
        return functionInfoDao.findById(functionId).get();
    }

    public List<FunctionInfo> findAll() {
        return functionInfoDao.findAll();
    }

    public Boolean checkIfExist(Long adminId,Long functionId){
        List<AdminFunctionRel> functionRelList = adminFunctionRelDao.findAllByAdminIdAndFunctionId(adminId,functionId);
        if(functionRelList.size()>0){
            return true;
        }
        return false;
    }


}
