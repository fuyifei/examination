package com.shinesend.web.service.admin;

import com.shinesend.base.service.SuperService;
import com.shinesend.web.dao.admin.AdminInfoDao;
import com.shinesend.web.dto.admin.AdminDTO;
import com.shinesend.web.entity.admin.AdminInfo;
import com.shinesend.web.util.QueryPageUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sqh
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class AdminInfoService extends SuperService<AdminInfo, Long, AdminInfoDao> {

    @Autowired
    private AdminInfoDao adminInfoDao;

    public AdminInfo save(AdminInfo adminInfo) {
        return insert(adminInfo);
    }

    /**
     * 查询有效用户
     *
     * @param adminId   标识
     * @param useStatus 是否删除
     * @return User
     */
    public AdminInfo getValidUser(Long adminId, boolean useStatus) {
        return adminInfoDao.findByAdminIdAndUseStatus(adminId, useStatus);
    }

    /**
     * 登录接口
     *
     * @param loginName    用户名
     * @param passwordHash 密码
     * @param useStatus    状态
     * @return
     */
    public AdminInfo login(String loginName, String passwordHash, boolean useStatus) {
        return adminInfoDao.findByLoginNameAndPasswordHashAndUseStatus(loginName, passwordHash, useStatus);
    }

    /**
     * 登录接口
     *
     * @param loginName 用户名
     * @param useStatus 状态
     * @return
     */
    public AdminInfo login(String loginName, boolean useStatus) {
        return adminInfoDao.findByLoginNameAndUseStatus(loginName, useStatus);
    }

    /**
     * 根据 userName 查重
     *
     * @param loginName 用户名
     * @return
     */
    public int countByAdminName(String loginName) {
        return adminInfoDao.countByLoginName(loginName);
    }

    /**
     * 根据 ID 和 状态查询用户是否存在
     *
     * @param adminLd   id
     * @param useStatus useStatus
     * @return 用户信息
     */
    public AdminInfo findByIdAndUseStatus(Long adminLd, boolean useStatus) {
        return adminInfoDao.findByAdminIdAndUseStatus(adminLd, useStatus);
    }


    public Page queryPage(AdminDTO adminDTO) {
        int currentNum = adminDTO.getPage().getCurrentNum();
        int pageSize = adminDTO.getPage().getPageSize();
        String name = adminDTO.getFullName();
        String mobile = adminDTO.getMobile();
        Long adminId = adminDTO.getAdminId();
        StringBuilder sqlSb = new StringBuilder();
        sqlSb.append(" select a.admin_id,a.login_name,a.full_name,a.mobile,a.use_status,a.data_created,a.qw_user_id,a.input_man_id, ");
        sqlSb.append(" b.full_name inputManName from CRM_ADMIN_INFO a,crm_admin_info b ");
        sqlSb.append("  where a.input_man_id = b.admin_id(+) ");
        if (StringUtils.isNotBlank(name)) {
            sqlSb.append(" and a.full_name like '%" + name + "%'");
        }
        if (StringUtils.isNotBlank(mobile)) {
            sqlSb.append(" and a.mobile like '" + mobile + "%' ");
        }
        if(adminId != null){
            sqlSb.append(" and a.admin_Id = " +adminId );
        }
        sqlSb.append(" order by a.admin_id desc ");
        sqlSb = QueryPageUtil.getQueryPageSql(sqlSb, pageSize, currentNum);
        List<Map<String, Object>> respList = new ArrayList<>();
        List<Object[]> objList = adminInfoDao.queryObjBySql(sqlSb.toString());
        objList.forEach(objects1 -> {
            Map<String, Object> map = new HashMap<>(8);
            map.put("adminId", objects1[0]);
            map.put("loginName", objects1[1]);
            map.put("fullName", objects1[2]);
            map.put("molile", objects1[3]);
            map.put("useStatus", objects1[4]);
            map.put("dataCreated", objects1[5]);
            map.put("qwUserId", objects1[6]);
            map.put("inputManId", objects1[7]);
            map.put("inputManName", objects1[8]);
            respList.add(map);
        });
        StringBuilder countSqlSb = new StringBuilder();
        countSqlSb.append("select count(*) from CRM_ADMIN_INFO a,crm_admin_info b  ");
        countSqlSb.append(" where a.input_man_id = b.admin_id(+) ");
        if (StringUtils.isNotBlank(name)) {
            countSqlSb.append(" and a.full_name like '%" + name + "%'");
        }
        if (StringUtils.isNotBlank(mobile)) {
            countSqlSb.append(" and a.mobile like '" + mobile + "%' ");
        }
        if(adminId != null){
            countSqlSb.append(" and a.admin_Id = " +adminId );
        }
        String objBySql = String.valueOf(adminInfoDao.getObjBySql(countSqlSb.toString()));
        PageRequest pageRequest = PageRequest.of(currentNum - 1, pageSize);
        return new PageImpl(respList, pageRequest, Long.parseLong(String.valueOf(objBySql)));
    }

    public void stopAdmin(List<Long> idList) {
        for (int i = 0; i < idList.size(); i++) {
            AdminInfo admin = adminInfoDao.findById(idList.get(i)).get();
            admin.setUseStatus(true);
            adminInfoDao.save(admin);
        }
    }

    public void startAdmin(List<Long> idList) {
        for (int i = 0; i < idList.size(); i++) {
            AdminInfo admin = adminInfoDao.findById(idList.get(i)).get();
            admin.setUseStatus(false);
            adminInfoDao.save(admin);
        }
    }
}
