package com.shinesend.web.service.mall;

import com.shinesend.base.service.SuperService;
import com.shinesend.web.dao.mall.GoodsInfoDao;
import com.shinesend.web.dto.mall.GoodsInfoDTO;
import com.shinesend.web.entity.mall.GoodsInfo;
import com.shinesend.web.util.QueryPageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Map;

@Service
public class GoodsInfoService extends SuperService<GoodsInfo, Long, GoodsInfoDao> {
    @Autowired
    private GoodsInfoDao goodsInfoDao;

    public Page queryPage(GoodsInfoDTO goodsInfoDTO) {
        int currentNum = goodsInfoDTO.getPage().getCurrentNum();
        int pageSize = goodsInfoDTO.getPage().getPageSize();
        StringBuilder sqlSb = new StringBuilder();
        sqlSb.append(" select * from GOODS_INFO order by id desc ");
        sqlSb = QueryPageUtil.getQueryPageSql(sqlSb, pageSize, currentNum);
        List<Map<String, Object>> respList = goodsInfoDao.queryMapBySql(sqlSb.toString());
        StringBuffer countSqlSb = new StringBuffer();
        countSqlSb.append(" select count(0) from GOODS_INFO ");
        String objBySql = String.valueOf(goodsInfoDao.getObjBySql(countSqlSb.toString()));
        PageRequest pageRequest = PageRequest.of(currentNum - 1, pageSize);
        return new PageImpl(respList, pageRequest, Long.parseLong(String.valueOf(objBySql)));
    }
}
