package com.shinesend.web.service.question;

import com.shinesend.base.service.SuperService;
import com.shinesend.web.dao.question.QuestionDao;
import com.shinesend.web.dto.question.QuestionDTO;
import com.shinesend.web.entity.question.Question;
import com.shinesend.web.util.QueryPageUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class QuestionService extends SuperService<Question, Long, QuestionDao> {
    @Autowired
    private QuestionDao questionDao;

    public Map<String, Object> findOneBySortId(Long sortId, Long userId) {
        StringBuffer sqlSb = new StringBuffer();
        sqlSb.append(" SELECT * FROM ( SELECT a.* FROM QUESTION a where a.sort_id = " + sortId);
        sqlSb.append(" and not exists (select 1 from ERROR_LOG b where a.question_id = b.question_id and b.USER_ID = " + userId + "  ) ");
        sqlSb.append(" ORDER BY dbms_random.value ) WHERE rownum = 1 ");
        List<Map<String, Object>> reMap = questionDao.queryMapBySql(sqlSb.toString());
        if (reMap.size() > 0 && reMap.get(0) != null) {
            return reMap.get(0);
        }
        return null;
    }

    public List<Map<String, Object>> findAllBySortId(Long sortId) {
        StringBuffer sqlSb = new StringBuffer();
        sqlSb.append(" SELECT * FROM ( SELECT a.* FROM QUESTION a where a.sort_id = " + sortId);
        sqlSb.append(" ORDER BY dbms_random.value ) WHERE rownum <= 135 ");
        return questionDao.queryMapBySql(sqlSb.toString());
    }

    public Map<String, Object> findOneErrorBySortId(Long sortId, Long userId) {
        StringBuffer sqlSb = new StringBuffer();
        sqlSb.append(" SELECT * FROM ( SELECT a.* FROM QUESTION a,ERROR_LOG b where a.sort_id = " + sortId);
        sqlSb.append(" and b.USER_ID = " + userId);
        sqlSb.append(" and a.question_id = b.question_id ");
        sqlSb.append(" ORDER BY dbms_random.value ) WHERE rownum = 1 ");
        List<Map<String, Object>> reMap = questionDao.queryMapBySql(sqlSb.toString());
        if (reMap.size() > 0 && reMap.get(0) != null) {
            return reMap.get(0);
        }
        return null;
    }

    public Page queryQuestionPage(QuestionDTO questionDTO) {
        int currentNum = questionDTO.getPage().getCurrentNum();
        int pageSize = questionDTO.getPage().getPageSize();
        StringBuilder sqlSb = new StringBuilder();
        sqlSb.append(" select * from Question where 1=1 ");
        Long sortId = questionDTO.getSortId();
        if (sortId != null) {
            sqlSb.append(" and SORT_ID = " + sortId);
        }
        String questionName = questionDTO.getQuestionStem();
        if(StringUtils.isNotBlank(questionName)){
            sqlSb.append(" and Question_Stem like '%"+questionName+"%'");
        }
        sqlSb.append(" order by QUESTION_ID desc ");
        sqlSb = QueryPageUtil.getQueryPageSql(sqlSb, pageSize, currentNum);
        List<Map<String, Object>> respList = questionDao.queryMapBySql(sqlSb.toString());
        StringBuffer countSqlSb = new StringBuffer();
        countSqlSb.append(" select count(0) from Question where 1=1 ");

        if (sortId != null) {
            countSqlSb.append(" and SORT_ID = " + sortId);
        }

        if(StringUtils.isNotBlank(questionName)){
            countSqlSb.append(" and Question_Stem like '%"+questionName+"%'");
        }
        String objBySql = String.valueOf(questionDao.getObjBySql(countSqlSb.toString()));
        PageRequest pageRequest = PageRequest.of(currentNum - 1, pageSize);
        return new PageImpl(respList, pageRequest, Long.parseLong(String.valueOf(objBySql)));
    }

}
