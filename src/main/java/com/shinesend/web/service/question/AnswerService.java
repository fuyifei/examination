package com.shinesend.web.service.question;

import com.shinesend.base.service.SuperService;
import com.shinesend.web.dao.question.AnswerDao;
import com.shinesend.web.entity.question.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AnswerService extends SuperService<Answer, Long, AnswerDao> {
    @Autowired
    private AnswerDao answerDao;

    public List<Answer> findAllByQuestionId(Long questionId){
        return answerDao.findAllByQuestionId(questionId);
    }
}
