package com.shinesend.web.service.question;

import com.shinesend.base.service.SuperService;
import com.shinesend.web.dao.question.ErrorLogDao;
import com.shinesend.web.entity.question.ErrorLog;
import org.springframework.stereotype.Service;

@Service
public class ErrorLogService extends SuperService<ErrorLog, Long, ErrorLogDao> {
}
