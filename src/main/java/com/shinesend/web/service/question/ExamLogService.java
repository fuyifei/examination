package com.shinesend.web.service.question;

import com.shinesend.base.service.SuperService;
import com.shinesend.web.dao.question.ExamLogDao;
import com.shinesend.web.entity.question.ExamLog;
import org.springframework.stereotype.Service;

@Service
public class ExamLogService extends SuperService<ExamLog, Long, ExamLogDao> {
}
