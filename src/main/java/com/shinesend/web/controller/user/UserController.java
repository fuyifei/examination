package com.shinesend.web.controller.user;

import com.shinesend.base.result.ApiResult;
import com.shinesend.miniapp.dto.UserInfoDTO;
import com.shinesend.miniapp.service.UserInfoService;
import com.shinesend.web.dto.IdsDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 小程序用户管理
 *
 * @author sqh
 */
@RestController
@RequestMapping("/web/user")
public class UserController {

    @Autowired
    private UserInfoService userInfoService;

    @PostMapping("/queryUser")
    public ApiResult queryUser(@RequestBody UserInfoDTO userInfoDTO) {

        return ApiResult.success(userInfoService.userQueryPage(userInfoDTO));
    }

    @PostMapping("/setShopHead")
    public ApiResult setShopHead(@RequestBody IdsDTO ids) {
        userInfoService.setShopHead(ids.getIds());
        return ApiResult.success();
    }

    @PostMapping("/cancelShopHead")
    public ApiResult cancelShopHead(@RequestBody IdsDTO ids) {
        userInfoService.cancelShopHead(ids.getIds());
        return ApiResult.success();
    }

    @RequestMapping("/startUseStatus")
    public ApiResult updateUseStatus(@RequestBody IdsDTO ids){
        userInfoService.startUseStatus(ids.getIds());
        return ApiResult.success();
    }
    @RequestMapping("/stopUseStatus")
    public ApiResult stopUseStatus(@RequestBody IdsDTO ids){
        userInfoService.stopUseStatus(ids.getIds());
        return ApiResult.success();
    }

}
