package com.shinesend.web.controller.admin;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.shinesend.base.result.ApiCode;
import com.shinesend.base.result.ApiResult;
import com.shinesend.base.util.JwtUtil;
import com.shinesend.base.util.PasswordUtil;
import com.shinesend.web.dao.admin.AdminInfoDao;
import com.shinesend.web.dto.IdsDTO;
import com.shinesend.web.dto.admin.AdminDTO;
import com.shinesend.web.dto.admin.AdminFunctionDTO;
import com.shinesend.web.dto.admin.LoginDTO;
import com.shinesend.web.entity.admin.AdminFunctionRel;
import com.shinesend.web.entity.admin.AdminInfo;
import com.shinesend.web.entity.admin.FunctionInfo;
import com.shinesend.web.service.admin.AdminFunctionRelService;
import com.shinesend.web.service.admin.AdminInfoService;
import com.shinesend.web.service.admin.FunctionInfoService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author sqh
 **/
@RestController
@RequestMapping("/web/adminInfo")
@Slf4j
public class AdminInfoController {
    @Autowired
    private AdminInfoService adminInfoService;
    @Autowired
    private AdminInfoDao adminInfoDao;
    @Autowired
    private FunctionInfoService functionInfoService;
    @Autowired
    private AdminFunctionRelService adminFunctionRelService;

    @PostMapping("/login")
    public ApiResult login(@RequestBody LoginDTO loginDTO) throws Exception {
        if (StringUtils.isBlank(loginDTO.getLoginName())) {
            log.error("登录请求时，字段 [loginName] 不能为空");
            return ApiResult.failure(ApiCode.PARAM_LACK);
        }
        if (StringUtils.isBlank(loginDTO.getPasswordHash())) {
            log.error("登录请求时，字段 [passwordHash] 不能为空");
            return ApiResult.failure(ApiCode.PARAM_LACK);
        }
        AdminInfo loginUser = adminInfoService.login(loginDTO.getLoginName(), false);
        if (loginUser == null) {
            return ApiResult.failure(ApiCode.USER_IS_NOT_EXISTS);
        }
        // 验证密码
        boolean valid = PasswordUtil.valid(loginDTO.getPasswordHash(), loginUser.getSalt(), loginUser.getPasswordHash());
        if (!valid) {
            return ApiResult.failure(ApiCode.USERNAME_PWD_NOT_MATCH);
        }

        Map<String, Object> respData = new HashMap<>(2);
        respData.put("adminInfo", loginUser);
        String token = JwtUtil.genToken(Integer.valueOf(String.valueOf(loginUser.getAdminId())), loginUser.getLoginName());
        respData.put("token", token);
        List<AdminFunctionRel> relList = adminFunctionRelService.findAllByAdminId(loginUser.getAdminId());
        List<FunctionInfo> menuList = new ArrayList<>();
        for (int i = 0; i < relList.size(); i++) {
            AdminFunctionRel rel = relList.get(i);
            Long functionId = rel.getFunctionId();
            FunctionInfo functionInfo = functionInfoService.findById(functionId);
            if(functionInfo.getFunctionTypeId() == 1){
                menuList.add(functionInfo);
            }

        }
        respData.put("menuList",menuList);
        return ApiResult.success(respData);
    }

    /**
     * 创建一个用户
     *
     * @param adminDTO
     * @return
     */
    @PostMapping("/save")
    public ApiResult save(@RequestBody AdminDTO adminDTO) {
        Long adminId = adminDTO.getAdminId();
        String qwUserId = adminDTO.getQwUserId();

        AdminInfo admin = new AdminInfo();
        if (adminId != null) {
            admin = adminInfoService.get(adminId);
            admin.setQwUserId(qwUserId);
            admin = adminInfoService.update(admin);
            return ApiResult.success(admin);
        } else {
            Optional<String> adminNameOp = Optional.ofNullable(adminDTO.getLoginName());
            Optional<String> fullNameOp = Optional.ofNullable(adminDTO.getFullName());
            Optional<String> mobileOp = Optional.ofNullable(adminDTO.getMobile());
            Optional<String> passwordHashOp = Optional.ofNullable(adminDTO.getPasswordHash());
            if (!adminNameOp.isPresent() || !fullNameOp.isPresent() || !mobileOp.isPresent() || !passwordHashOp.isPresent()) {
                return ApiResult.failure(ApiCode.PARAM_IS_BLANK);
            }
            String fullName = adminDTO.getFullName();
            String loginName = adminDTO.getLoginName();
            String mobile = adminDTO.getMobile();
            if(loginName.length()>16){
                return ApiResult.failure(" 登录名过长！ ");
            }
            if(fullName.length()>16){
                return ApiResult.failure(" 用户名过长！ ");
            }
            if(mobile.length()>11){
                return ApiResult.failure(" 请输入正确手机号 ");
            }
            // 查重
            int count = adminInfoService.countByAdminName(adminNameOp.get());
            if (count > 0) {
                return ApiResult.failure(ApiCode.ADMIN_EXISTS);
            }
            try {
                admin.setLoginName(adminNameOp.get());
                admin.setQwUserId(qwUserId);
                admin.setFullName(fullNameOp.get());
                String salt = PasswordUtil.randomSalt();
                String passwordHash = PasswordUtil.generatePassword(passwordHashOp.get(), salt);
                admin.setSalt(salt);
                admin.setUseStatus(false);
                admin.setMobile(mobileOp.get());
                admin.setPasswordHash(passwordHash);
                admin = adminInfoService.save(admin);
                return ApiResult.success(admin);
            } catch (NoSuchAlgorithmException e) {
                log.error("创建用户失败：{}", e.getMessage(), e);
                return ApiResult.failure(ApiCode.UNKNOWN_ERROR);
            }
        }
    }

    /**
     * 逻辑删除
     *
     * @return
     */
    @PostMapping("/delete")
    public ApiResult delete(@RequestBody List<Long> ids) {
        Optional<List<Long>> idsOp = Optional.ofNullable(ids);
        if (!idsOp.isPresent()) {
            return ApiResult.failure(ApiCode.PARAM_IS_BLANK);
        }
        for (Long id : ids) {
            Optional<AdminInfo> manageUserOp = Optional.ofNullable(adminInfoService.findByIdAndUseStatus(id, false));
            if (manageUserOp.isPresent()) {
                AdminInfo manageUser = manageUserOp.get();
                manageUser.setUseStatus(false);
                adminInfoService.update(manageUser);
            }
        }
        return ApiResult.success();
    }


    @PostMapping("/get")
    public ApiResult get(@RequestBody Long id) {
        AdminInfo manageUser = adminInfoService.get(id);
        if (manageUser != null) {
            return ApiResult.success(manageUser);
        }
        return ApiResult.failure(ApiCode.USERNAME_NOT_FOUND);
    }

    @PostMapping("/queryAllFunction")
    public ApiResult queryAllFunction() {

        return ApiResult.success(functionInfoService.findAll());
    }

    @PostMapping("/setAdminFunction")
    public ApiResult setAdminFunction(@RequestBody AdminFunctionDTO adminFunctionDTO, @RequestHeader("token") String token) {
        DecodedJWT verify = JwtUtil.verifySys(token);
        Long inputManId = verify.getClaim("adminId").asLong();
        List<Long> ids = adminFunctionDTO.getIds();
        Long adminId = adminFunctionDTO.getAdminId();
        List<AdminFunctionRel> relList = adminFunctionRelService.findAllByAdminId(adminId);
        for (int i = 0; i < relList.size(); i++) {
            adminFunctionRelService.delete(relList.get(i).getId());
        }
        for (int i = 0; i < ids.size(); i++) {
            Boolean bo = functionInfoService.checkIfExist(adminId,ids.get(i));
            if(!bo){
                AdminFunctionRel adminFunctionRel = new AdminFunctionRel();
                adminFunctionRel.setAdminId(adminId);
                adminFunctionRel.setFunctionId(ids.get(i));
                FunctionInfo function = functionInfoService.findById(ids.get(i));
                Long parentId = function.getParentId();
                if(parentId != 0 && !functionInfoService.checkIfExist(adminId,parentId)){
                    AdminFunctionRel adminFunctionRel1 = new AdminFunctionRel();
                    adminFunctionRel1.setAdminId(adminId);
                    adminFunctionRel1.setFunctionId(parentId);
                    adminFunctionRel1.setInputManId(inputManId);
                    adminFunctionRelService.insert(adminFunctionRel1);
                }
                adminFunctionRel.setInputManId(inputManId);
                adminFunctionRelService.insert(adminFunctionRel);
            }

        }

        return ApiResult.success();
    }
    @PostMapping("/queryAdminPage")
    public ApiResult queryAdminPage(@RequestBody AdminDTO adminDTO){
        return ApiResult.success(adminInfoService.queryPage(adminDTO));
    }

    @PostMapping("/stopAdmin")
    public ApiResult stopAdmin(@RequestBody IdsDTO idsDTO){
        adminInfoService.stopAdmin(idsDTO.getIds());
        return ApiResult.success();
    }
    @PostMapping("/startAdmin")
    public ApiResult startAdmin(@RequestBody IdsDTO idsDTO){
        adminInfoService.startAdmin(idsDTO.getIds());
        return ApiResult.success();
    }

    @PostMapping("/getFunctionByAdminIdAndParentId")
    public ApiResult getFunctionByAdminIdAndParentId(@RequestBody AdminFunctionDTO adminFunctionDTO ){

        return ApiResult.success(adminFunctionRelService.findAllByAdminIdAndParentIdAll(adminFunctionDTO.getAdminId(),adminFunctionDTO.getParentId()));
    }

    @PostMapping("/getMenu")
    public ApiResult getMenu(@RequestHeader("token") String token){
        DecodedJWT verify = JwtUtil.verifySys(token);
        Long inputManId = verify.getClaim("adminId").asLong();
        List<AdminFunctionRel> relList = adminFunctionRelService.findAllByAdminId(inputManId);
        Map result = new HashMap();
        List<Map<String,Object>> menuList = new ArrayList<>();
        for (int i = 0; i < relList.size(); i++) {
            AdminFunctionRel rel = relList.get(i);
            Long functionId = rel.getFunctionId();
            FunctionInfo functionInfo = functionInfoService.findById(functionId);

            if(functionInfo.getFunctionTypeId() == 1 && functionInfo.getParentId() == 0){
                String path = functionInfo.getFunctionUrl();
                String name = functionInfo.getFunctionEName();
                String icon = functionInfo.getFunctionIcon();
                Map menu = new HashMap();
                menu.put("name",name);
                menu.put("path",path);
                menu.put("icon",icon);
                List<Map<String,Object>> children = adminFunctionRelService.findAllByAdminIdAndParentId(inputManId,functionInfo.getFunctionId());
                menu.put("children",children);
                menuList.add(menu);
            }

        }
        result.put("menuList",menuList);
        return ApiResult.success(result);
    }



    @PostMapping("/getMenuByAdminId")
    public ApiResult getMenuByAdminId(@RequestBody AdminDTO adminDTO){
        Long adminId = adminDTO.getAdminId();
        List<AdminFunctionRel> relList = adminFunctionRelService.findAllByAdminId(adminId);
        Map result = new HashMap();
        List<String> functionIdArr = new ArrayList<>();
//        List<Map<String,Object>> menuList = new ArrayList<>();
        for (int i = 0; i < relList.size(); i++) {
            AdminFunctionRel rel = relList.get(i);
            Long functionId = rel.getFunctionId();
            FunctionInfo functionInfo = functionInfoService.findById(functionId);

            if( functionInfo.getParentId() != 0){
                String path = functionInfo.getFunctionUrl();
                String name = functionInfo.getFunctionEName();
                String icon = functionInfo.getFunctionIcon();
//                Map menu = new HashMap();
            functionIdArr.add(functionInfo.getFunctionId()+"");
//                menu.put("name",name);
//                menu.put("path",path);
//                menu.put("icon",icon);
//                List<Map<String,Object>> children = adminFunctionRelService.findAllByAdminIdAndParentId(adminId,functionInfo.getFunctionId());
//                menu.put("children",children);
//                menuList.add(menu);
            }

        }
        result.put("menuList",functionIdArr);
        return ApiResult.success(result);
    }

    @PostMapping("/updatePsd")
    public ApiResult updatePsd (@RequestBody AdminDTO adminDTO) throws Exception{
        Long adminId = adminDTO.getAdminId();
        String password = adminDTO.getPasswordHash();
        String salt = PasswordUtil.randomSalt();
        String passwordHash = PasswordUtil.generatePassword(password, salt);
        AdminInfo admin = adminInfoDao.findById(adminId).get();
        admin.setPasswordHash(passwordHash);
        admin.setSalt(salt);
        adminInfoDao.save(admin);
        return ApiResult.success();
    }

}
