package com.shinesend.web.controller.question;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.shinesend.base.result.ApiResult;
import com.shinesend.base.util.JwtUtil;
import com.shinesend.web.dto.IdsDTO;
import com.shinesend.web.dto.question.AnswerDTO;
import com.shinesend.web.dto.question.QuestionDTO;
import com.shinesend.web.entity.question.Answer;
import com.shinesend.web.entity.question.Question;
import com.shinesend.web.service.question.AnswerService;
import com.shinesend.web.service.question.QuestionService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 试题管理
 *
 * @author sqh
 */
@RestController
@RequestMapping("/web/question")
public class WebQuestionController {

    @Autowired
    private QuestionService questionService;
    @Autowired
    private AnswerService answerService;

    @PostMapping("/addQuestion")
    public ApiResult addQuestion(@RequestBody QuestionDTO questionDTO, @RequestHeader("token") String token) {
        DecodedJWT verify = JwtUtil.verifySys(token);
        Long inputManId = verify.getClaim("adminId").asLong();
        String checkResult = checkQuestionForm(questionDTO);
        if (checkResult != null) {
            return ApiResult.failure(checkResult);
        }
        Question question = new Question();
        question.setInputManId(inputManId);
        question.setQuestionStem(questionDTO.getQuestionStem());
        question.setSortId(questionDTO.getSortId());
        question.setQuestionAnalysis(questionDTO.getQuestionAnalysis());
        questionService.insert(question);
        return ApiResult.success();
    }

    @PostMapping("/updateQuestion")
    public ApiResult updateQuestion(@RequestBody QuestionDTO questionDTO, @RequestHeader("token") String token) {
        DecodedJWT verify = JwtUtil.verifySys(token);
        Long inputManId = verify.getClaim("adminId").asLong();
        String checkResult = checkQuestionForm(questionDTO);
        if (checkResult != null) {
            return ApiResult.failure(checkResult);
        }
        Long questionId = questionDTO.getQuestionId();
        Question question = questionService.get(questionId);
        question.setUpdateManId(inputManId);
        question.setQuestionStem(questionDTO.getQuestionStem());
        question.setSortId(questionDTO.getSortId());
        question.setQuestionAnalysis(questionDTO.getQuestionAnalysis());
        questionService.update(question);
        return ApiResult.success();
    }

    private String checkQuestionForm(QuestionDTO questionDTO) {
        String questionStem = questionDTO.getQuestionStem();
        if (!StringUtils.isNotBlank(questionStem)) {
            return "请输入题干";
        }
        Long sortId = questionDTO.getSortId();
        if (sortId == null) {
            return "请选择题目类别";
        }
        String analysis = questionDTO.getQuestionAnalysis();
        if (!StringUtils.isNotBlank(analysis)) {
            return "请输入解析";
        }
        return null;
    }

    @PostMapping("/deleteQuestion")
    public ApiResult deleteQuestion(@RequestBody IdsDTO idsDTO) {
        List<Long> idList = idsDTO.getIds();
        for (int i = 0; i < idList.size(); i++) {
            List<Answer> answersList = answerService.findAllByQuestionId(idList.get(i));
            for (int j = 0; j < answersList.size(); j++) {
                answerService.delete(answersList.get(j).getAnswerId());
            }
            questionService.delete(idList.get(i));
        }
        return ApiResult.success();
    }

    @PostMapping("/addAnswer")
    public ApiResult addAnswer(@RequestBody AnswerDTO answerDTO, @RequestHeader("token") String token) {
        DecodedJWT verify = JwtUtil.verifySys(token);
        Long inputManId = verify.getClaim("adminId").asLong();
        String checkResult = checkAnswerForm(answerDTO);
        if (checkResult != null) {
            return ApiResult.failure(checkResult);
        }
        Answer answer = new Answer();
        answer.setCorrectFlag(answerDTO.getCorrectFlag());
        answer.setAnswerContent(answerDTO.getAnswerContent());
        answer.setQuestionId(answerDTO.getQuestionId());
        answer.setInputManId(inputManId);
        answerService.insert(answer);
        return ApiResult.success();
    }

    @PostMapping("/updateAnswer")
    public ApiResult updateAnswer(@RequestBody AnswerDTO answerDTO, @RequestHeader("token") String token) {
        DecodedJWT verify = JwtUtil.verifySys(token);
        Long inputManId = verify.getClaim("adminId").asLong();
        String checkResult = checkAnswerForm(answerDTO);
        if (checkResult != null) {
            return ApiResult.failure(checkResult);
        }
        Long answerId = answerDTO.getAnswerId();
        Answer answer = answerService.get(answerId);
        answer.setCorrectFlag(answerDTO.getCorrectFlag());
        answer.setAnswerContent(answerDTO.getAnswerContent());
        answer.setQuestionId(answerDTO.getQuestionId());
        answer.setUpdateManId(inputManId);
        answerService.update(answer);
        return ApiResult.success();
    }

    private String checkAnswerForm(AnswerDTO answerDTO) {
        String content = answerDTO.getAnswerContent();
        if (!StringUtils.isNotBlank(content)) {
            return "请输入答案内容";
        }
        Integer correctFlag = answerDTO.getCorrectFlag();
        if (correctFlag == null) {
            return "请选择正确标识";
        }
        Long questionId = answerDTO.getQuestionId();
        if (questionId == null) {
            return "没有关联问题";
        }
        return null;
    }

    @PostMapping("/deleteAnswer")
    public ApiResult deleteAnswer(@RequestBody IdsDTO idsDTO) {
        List<Long> idList = idsDTO.getIds();
        for (int i = 0; i < idList.size(); i++) {
            answerService.delete(idList.get(i));
        }
        return ApiResult.success();
    }

    @PostMapping("/queryQuestion")
    public ApiResult queryQuestionPage(@RequestBody QuestionDTO questionDTO) {

        return ApiResult.success(questionService.queryQuestionPage(questionDTO));
    }

    @PostMapping("/queryAnswerByQuestionId")
    public ApiResult queryAnswerByQuestionId(@RequestBody QuestionDTO questionDTO) {

        return ApiResult.success(answerService.findAllByQuestionId(questionDTO.getQuestionId()));
    }
}
