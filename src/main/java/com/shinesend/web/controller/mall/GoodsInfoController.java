package com.shinesend.web.controller.mall;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.shinesend.base.result.ApiResult;
import com.shinesend.base.util.JwtUtil;
import com.shinesend.miniapp.entity.FileInfo;
import com.shinesend.miniapp.service.FileInfoService;
import com.shinesend.web.dto.IdsDTO;
import com.shinesend.web.dto.mall.GoodsInfoDTO;
import com.shinesend.web.entity.mall.GoodsInfo;
import com.shinesend.web.service.mall.GoodsInfoService;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/web/goods")
public class GoodsInfoController {

    @Autowired
    private GoodsInfoService goodsInfoService;
    @Autowired
    private FileInfoService fileInfoService;

    @PostMapping("/addGoods")
    public ApiResult addGoods(@RequestBody GoodsInfoDTO goodsInfoDTO, @RequestHeader("token") String token) {
        DecodedJWT verify = JwtUtil.verifySys(token);
        Long inputManId = verify.getClaim("adminId").asLong();
        String checkResult = checkQuestionForm(goodsInfoDTO);
        if (checkResult != null) {
            return ApiResult.failure(checkResult);
        }
        Optional<String> imgBase64Op = Optional.ofNullable(goodsInfoDTO.getImgBase64());
        Optional<String> imgTypeOp = Optional.ofNullable(goodsInfoDTO.getImgType());
        FileInfo fileInfoSaved = null;
        if (imgBase64Op.isPresent()) {
            FileInfo fileInfo = new FileInfo();
            fileInfo.setFileBytes(Base64.decodeBase64(imgBase64Op.get()));
            fileInfo.setFileType(imgTypeOp.get());
            fileInfoSaved = fileInfoService.insert(fileInfo);
        }
        GoodsInfo goodsInfo = new GoodsInfo();
        if (fileInfoSaved != null) {
            goodsInfo.setGoodsImg(fileInfoSaved.getId());
        } else {
            goodsInfo.setGoodsImg(goodsInfoDTO.getGoodsImg());
        }
        goodsInfo.setGoodsName(goodsInfoDTO.getGoodsName());
        goodsInfo.setDescribe(goodsInfoDTO.getDescribe());
        goodsInfo.setSaPrice(goodsInfoDTO.getSaPrice());
        goodsInfo.setDisPrice(goodsInfoDTO.getDisPrice());
        goodsInfo.setGoodsImg(goodsInfoDTO.getGoodsImg());
        goodsInfo.setInputManId(inputManId);
        goodsInfoService.insert(goodsInfo);
        return ApiResult.success();
    }

    @PostMapping("/updateGoods")
    public ApiResult updateGoods(@RequestBody GoodsInfoDTO goodsInfoDTO, @RequestHeader("token") String token) {
        DecodedJWT verify = JwtUtil.verifySys(token);
        Long inputManId = verify.getClaim("adminId").asLong();
        String checkResult = checkQuestionForm(goodsInfoDTO);
        if (checkResult != null) {
            return ApiResult.failure(checkResult);
        }
        Optional<String> imgBase64Op = Optional.ofNullable(goodsInfoDTO.getImgBase64());
        Optional<String> imgTypeOp = Optional.ofNullable(goodsInfoDTO.getImgType());
        FileInfo fileInfoSaved = null;
        if (imgBase64Op.isPresent()) {
            FileInfo fileInfo = new FileInfo();
            fileInfo.setFileBytes(Base64.decodeBase64(imgBase64Op.get()));
            fileInfo.setFileType(imgTypeOp.get());
            fileInfoSaved = fileInfoService.insert(fileInfo);
        }
        GoodsInfo goodsInfo = goodsInfoService.get(goodsInfoDTO.getId());
        if (fileInfoSaved != null) {
            goodsInfo.setGoodsImg(fileInfoSaved.getId());
        } else {
            goodsInfo.setGoodsImg(goodsInfoDTO.getGoodsImg());
        }
        goodsInfo.setGoodsName(goodsInfoDTO.getGoodsName());
        goodsInfo.setDescribe(goodsInfoDTO.getDescribe());
        goodsInfo.setDisPrice(goodsInfoDTO.getDisPrice());
        goodsInfo.setSaPrice(goodsInfoDTO.getSaPrice());
        goodsInfo.setGoodsImg(goodsInfoDTO.getGoodsImg());
        goodsInfo.setUpdateManId(inputManId);
        return ApiResult.success();
    }

    private String checkQuestionForm(GoodsInfoDTO goodsInfoDTO) {
        String goodsName = goodsInfoDTO.getGoodsName();
        if (!StringUtils.isNotBlank(goodsName)) {
            return "请输入商品名称";
        }
        String describe = goodsInfoDTO.getDescribe();
        if (!StringUtils.isNotBlank(describe)) {
            return "请输入描述";
        }
        String saPrice = goodsInfoDTO.getSaPrice();
        if (!StringUtils.isNotBlank(saPrice)) {
            return "请输入销售价";
        }
        String disPrice = goodsInfoDTO.getDisPrice();
        if (!StringUtils.isNotBlank(disPrice)) {
            return "请输入折扣价";
        }
        return null;
    }

    @PostMapping("/deleteGoods")
    public ApiResult deleteGoods(@RequestBody IdsDTO idsDTO) {
        List<Long> idList = idsDTO.getIds();
        for (int i = 0; i < idList.size(); i++) {
            goodsInfoService.delete(idList.get(i));
        }
        return ApiResult.success();
    }
    @PostMapping("/queryGoods")
    public ApiResult queryGoods(@RequestBody GoodsInfoDTO goodsInfoDTO) {

        return ApiResult.success(goodsInfoService.queryPage(goodsInfoDTO));
    }
}
