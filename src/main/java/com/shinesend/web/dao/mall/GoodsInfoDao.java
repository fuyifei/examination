package com.shinesend.web.dao.mall;

import com.shinesend.base.repository.BaseRepository;
import com.shinesend.web.entity.mall.GoodsInfo;

public interface GoodsInfoDao extends BaseRepository<GoodsInfo, Long> {
}
