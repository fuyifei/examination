package com.shinesend.web.dao.question;

import com.shinesend.base.repository.BaseRepository;
import com.shinesend.web.entity.question.Answer;

import java.util.List;

public interface AnswerDao extends BaseRepository<Answer, Long> {

    List<Answer> findAllByQuestionId(Long questionId);
}
