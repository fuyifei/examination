package com.shinesend.web.dao.question;

import com.shinesend.base.repository.BaseRepository;
import com.shinesend.web.entity.question.ExamLog;

public interface ExamLogDao extends BaseRepository<ExamLog, Long> {
}
