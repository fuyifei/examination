package com.shinesend.web.dao.question;

import com.shinesend.base.repository.BaseRepository;
import com.shinesend.web.entity.question.Question;

import java.util.List;

public interface QuestionDao extends BaseRepository<Question, Long> {

    List<Question> findAllBySortId(Long sortId);
}
