package com.shinesend.web.dao.question;

import com.shinesend.base.repository.BaseRepository;
import com.shinesend.web.entity.question.ErrorLog;

public interface ErrorLogDao extends BaseRepository<ErrorLog, Long> {
}
