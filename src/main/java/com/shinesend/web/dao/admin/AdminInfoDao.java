package com.shinesend.web.dao.admin;

import com.shinesend.base.repository.BaseRepository;
import com.shinesend.web.entity.admin.AdminInfo;

/**
 * @author sqh
 **/
public interface AdminInfoDao extends BaseRepository<AdminInfo, Long> {
    /**
     * 查询用户是否存在
     *
     * @param loginName 登录名
     * @param psdHash   密码 Hash
     * @param useStatus 是否停用
     * @return ManageUser
     */
    AdminInfo findByLoginNameAndPasswordHashAndUseStatus(String loginName, String psdHash, boolean useStatus);

    /**
     * 查询一个有效的用户
     *
     * @param adminId   标识
     * @param useStatus 是否删除
     * @return User
     */
    AdminInfo findByAdminIdAndUseStatus(Long adminId, boolean useStatus);

    /**
     * 查询用户是否存在
     *
     * @param loginName 登录名
     * @param useStatus 是否停用
     * @return ManageUser
     */
    AdminInfo findByLoginNameAndUseStatus(String loginName, boolean useStatus);

    /**
     * 查询是否有重复的 UserName
     *
     * @param loginName 登录用户名
     * @return
     */
    int countByLoginName(String loginName);
}
