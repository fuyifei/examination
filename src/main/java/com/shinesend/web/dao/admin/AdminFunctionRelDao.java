package com.shinesend.web.dao.admin;

import com.shinesend.base.repository.BaseRepository;
import com.shinesend.web.entity.admin.AdminFunctionRel;

import java.util.List;

public interface AdminFunctionRelDao extends BaseRepository<AdminFunctionRel, Long> {

    List<AdminFunctionRel> findAllByAdminIdOrderByFunctionId(Long adminId);
    List<AdminFunctionRel> findAllByAdminIdAndFunctionId(Long adminId, Long functionId);
}
