package com.shinesend.web.dao.admin;

import com.shinesend.base.repository.BaseRepository;
import com.shinesend.web.entity.admin.FunctionInfo;

import java.util.List;

public interface FunctionInfoDao extends BaseRepository<FunctionInfo, Long> {

}
