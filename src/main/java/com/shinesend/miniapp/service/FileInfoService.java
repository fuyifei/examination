package com.shinesend.miniapp.service;

import com.shinesend.base.service.SuperService;
import com.shinesend.miniapp.dao.FileInfoDao;
import com.shinesend.miniapp.entity.FileInfo;
import org.springframework.stereotype.Service;

@Service
public class FileInfoService extends SuperService<FileInfo, Long, FileInfoDao> {
}
