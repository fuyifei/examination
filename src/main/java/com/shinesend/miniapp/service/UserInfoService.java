package com.shinesend.miniapp.service;

import com.shinesend.base.config.ConfigService;
import com.shinesend.base.service.SuperService;
import com.shinesend.miniapp.dao.FileInfoDao;
import com.shinesend.miniapp.dao.UserInfoDao;
import com.shinesend.miniapp.dto.UserInfoDTO;
import com.shinesend.miniapp.entity.FileInfo;
import com.shinesend.miniapp.entity.UserInfo;
import com.shinesend.miniapp.util.MobileDTO;
import com.shinesend.miniapp.util.WXBizDataCrypt;
import com.shinesend.web.util.QueryPageUtil;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserInfoService extends SuperService<UserInfo, Long, UserInfoDao> {
    @Autowired
    private UserInfoDao userInfoDao;
    @Autowired
    private ConfigService configService;
    @Autowired
    private FileInfoDao fileInfoDao;

    public UserInfo login(UserInfoDTO userInfoDTO) {
        String code = userInfoDTO.getCode();
        String openIdRespStr = geiOpenId(code);
        JSONObject openIdJson = new JSONObject(openIdRespStr);
        String wxOpenId = openIdJson.getString("openid");
        String sessionKey = openIdJson.getString("session_key");
        List<UserInfo> userList = userInfoDao.findAllByWxOpenId(wxOpenId);
        UserInfo user = new UserInfo();
        if (userList.size() == 0) {
            user.setWxOpenId(wxOpenId);
            user.setMobile(userInfoDTO.getMobile());
            user.setUserName(userInfoDTO.getUserName());
            user.setUseStatus(0);
            user = userInfoDao.save(user);
        } else {
            user = userList.get(0);
        }
        user.setSessionKey(sessionKey);
        return user;
    }

    public List<Map<String, Object>> queryGrapeUserByMobile(String mobile) {
        List<Map<String, Object>> mapList = new ArrayList<>();
        StringBuilder sqlSb = new StringBuilder();
        sqlSb.append(" select a.userId,a.userName,a.deptId,b.companyName,a.mobileNo,decode(a.sex,1,'男',2,'女') sex ");
        sqlSb.append(" from sys_user a,sys_company b ");
        sqlSb.append(" where a.deptId=b.companyId and a.mobileNo = '" + mobile + "' ");
        List<Object[]> objList = userInfoDao.queryObjBySql(sqlSb.toString());
        for (int i = 0; i < objList.size(); i++) {
            Object[] item = objList.get(i);
            if (item != null) {
                Map<String, Object> map = new HashMap<>();
                map.put("userId", item[0]);
                map.put("userName", item[1]);
                map.put("shopId", item[2]);
                map.put("shopName", item[3]);
                map.put("mobile", item[4]);
                map.put("sex", item[5]);
                mapList.add(map);
            }
        }
        return mapList;
    }

    public String geiOpenId(String code) {
        JSONObject jsonObject = null;
        StringBuffer buffer = new StringBuffer();
        String appId = configService.getConfigValue("wx.apiId");
        String secret = configService.getConfigValue("wx.appSecret");
        try {
            String urlStr = "https://api.weixin.qq.com/sns/jscode2session?appid=" + appId + "&secret=" + secret + "" +
                    "&js_code=" + code + "&grant_type=authorization_code";
            URL url = new URL(urlStr);
            HttpsURLConnection httpUrlConn = (HttpsURLConnection) url.openConnection();
            httpUrlConn.setDoOutput(true);
            httpUrlConn.setDoInput(true);
            httpUrlConn.setUseCaches(false);
            // 设置请求方式（GET/POST）
            httpUrlConn.setRequestMethod("GET");
            // 设置连接主机服务器的超时时间：15000毫秒
            httpUrlConn.setConnectTimeout(15000);
            // 设置读取远程返回的数据时间：60000毫秒
            httpUrlConn.setReadTimeout(60000);
            httpUrlConn.connect();

            // 将返回的输入流转换成字符串
            InputStream inputStream = httpUrlConn.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "utf-8");
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            String str = null;
            while ((str = bufferedReader.readLine()) != null) {
                buffer.append(str);
            }
            bufferedReader.close();
            inputStreamReader.close();
            // 释放资源
            inputStream.close();
            inputStream = null;
            httpUrlConn.disconnect();
            System.out.println(buffer.toString());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return buffer.toString();
    }

    public UserInfo savePicture(String url, UserInfo userInfo) {
        try {
            URL urlPath = new URL(url);
            URLConnection uc = urlPath.openConnection();
            uc.connect();
            InputStream in = uc.getInputStream();
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            byte[] buffer = new byte[2048];
            int n = 0;
            while (-1 != (n = in.read(buffer))) {
                out.write(buffer, 0, n);
            }
            byte[] bytes = out.toByteArray();
            String imgBase64 = new String(Base64.encodeBase64(bytes));
            HttpURLConnection urlConnection = (HttpURLConnection) urlPath.openConnection();
            urlConnection.connect();
            InputStream inType = urlConnection.getInputStream();
            String type = HttpURLConnection.guessContentTypeFromStream(new BufferedInputStream(inType));
            FileInfo fileInfoSaved = null;
            if (!"".equals(imgBase64)) {
                FileInfo fileInfo = new FileInfo();
                fileInfo.setFileBytes(Base64.decodeBase64(imgBase64));
                fileInfo.setFileType(type.substring(type.indexOf("/") + 1));
                fileInfoSaved = fileInfoDao.save(fileInfo);
                userInfo.setImgId(fileInfoSaved.getId());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userInfo;
    }

    public String decryptWxUserPhoneNumber(MobileDTO mobileDTO) {
        String encryptedData = mobileDTO.getEncryptedData();
        Long userId = mobileDTO.getUserId();
        String iv = mobileDTO.getIv();
        String sessionKey = mobileDTO.getSessionKey();
        try {
            String userInfo = WXBizDataCrypt.decryptDat(encryptedData, iv, sessionKey);
            JSONObject userInfoJson = new JSONObject(userInfo);
            String mobile = userInfoJson.getString("phoneNumber");
            UserInfo user = userInfoDao.findById(userId).get();
            user.setMobile(mobile);
            userInfoDao.save(user);
            return mobile;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private Map<String, Object> getUserShopId(Long userId) {
        StringBuilder sqlSB = new StringBuilder();
        sqlSB.append(" select a.deptId,b.companyname from sys_user a,sys_company b where a.deptId = b.companyid and a.userId = " + userId);
        List<Object[]> objList = userInfoDao.queryObjBySql(sqlSB.toString());
        Map<String, Object> map = new HashMap<>();
        if (objList.size() > 0) {
            Object[] item = objList.get(0);
            map.put("shopId", item[0]);
            map.put("shopName", item[1]);
        }
        return map;
    }

    public Page userQueryPage(UserInfoDTO userInfoDTO) {
        int currentNum = userInfoDTO.getPage().getCurrentNum();
        int pageSize = userInfoDTO.getPage().getPageSize();
        String userNameQuery = userInfoDTO.getUserName();
        Integer headFlag = userInfoDTO.getHeadFlag();
        Integer useStatus = userInfoDTO.getUseStatus();
        Long grapeUserId = userInfoDTO.getGrapeUserId();
        String grapeUserName = userInfoDTO.getGrapeUserName();
        StringBuilder sqlSb = new StringBuilder();
        sqlSb.append(" select a.id, a.user_name, a.mobile, a.qy_user_id,");
        sqlSb.append(" c.userid,c.username,c.deptid,d.companyshortname,d.companyname,nvl(a.head_flag,0) ");
        sqlSb.append(" ,nvl(a.use_status,0) ");
        sqlSb.append(" from crm_app_user_info a, crm_grape_app_user_relation b,sys_user c,sys_company d ");
        sqlSb.append(" where b.app_user_id = a.id ");
        sqlSb.append(" and b.grape_user_id = c.userid ");
        sqlSb.append(" and c.deptid =d.companyid ");
        sqlSb = getWheres(sqlSb, userNameQuery, headFlag,useStatus,grapeUserId,grapeUserName);
        sqlSb.append(" order by b.id desc ");
        sqlSb = QueryPageUtil.getQueryPageSql(sqlSb, pageSize, currentNum);
        List<Object[]> objs = userInfoDao.queryObjBySql(sqlSb.toString());
        List<Map<String, Object>> respList = new ArrayList<>();
        objs.forEach(objects1 -> {
            Map<String, Object> map = new HashMap<>();
            map.put("userId", objects1[0]);
            map.put("userName", objects1[1]);
            map.put("mobile", objects1[2]);
            map.put("qyUserId", objects1[3]);
            map.put("grapeUserId", objects1[4]);
            map.put("grapeUserName", objects1[5]);
            map.put("shopId", objects1[6]);
            map.put("shopShortName", objects1[7]);
            map.put("shopName", objects1[8]);
            map.put("headFlag", objects1[9]);
            map.put("useStatus", objects1[10]);
            respList.add(map);
        });
        StringBuilder sqlSbCount = new StringBuilder();
        sqlSbCount.append(" select count(*) from crm_app_user_info a, crm_grape_app_user_relation b,sys_user c,sys_company d ");
        sqlSbCount.append("where b.app_user_id = a.id ");
        sqlSbCount.append(" and b.grape_user_id = c.userid ");
        sqlSbCount.append(" and c.deptid =d.companyid ");
        sqlSbCount = getWheres(sqlSbCount, userNameQuery, headFlag,useStatus,grapeUserId,grapeUserName);
        String objBySql = String.valueOf(userInfoDao.getObjBySql(sqlSbCount.toString()));
        PageRequest pageRequest = PageRequest.of(currentNum - 1, pageSize);

        return new PageImpl(respList, pageRequest, Long.parseLong(String.valueOf(objBySql)));
    }

    private StringBuilder getWheres(StringBuilder sqlSb, String userNameQuery, Integer headFlag,Integer useStatus,Long grapeUserId,String grapeUserName) {
        if (StringUtils.isNotBlank(userNameQuery)) {
            sqlSb.append(" and (a.user_name like '%" + userNameQuery + "%'");
            sqlSb.append(" or c.username like '%" + userNameQuery + "%'");
            if (StringUtils.isNumeric(userNameQuery)) {
                sqlSb.append(" or a.id = " + userNameQuery);
                sqlSb.append(" or c.userId = " + userNameQuery);
                sqlSb.append(" or c.deptId = " + userNameQuery);
            }
            sqlSb.append(" or a.mobile like '" + userNameQuery + "%' )");
        }
        if (headFlag != null) {
            sqlSb.append(" and a.head_flag = " + headFlag);
        }
        if(useStatus != null){
            sqlSb.append(" and a.use_Status = " + useStatus);
        }
        if(grapeUserId != null){
            sqlSb.append(" and b.grape_user_id = " +grapeUserId );
        }
        if(StringUtils.isNotBlank(grapeUserName)){
            sqlSb.append(" and c.username like '%"+grapeUserName+"%' ");
        }
        return sqlSb;
    }

    public void setShopHead(List<Long> idList) {
        for (int i = 0; i < idList.size(); i++) {
            UserInfo userInfo = userInfoDao.findById(idList.get(i)).get();
            userInfo.setHeadFlag(1);
            userInfoDao.save(userInfo);
        }
    }

    public void cancelShopHead(List<Long> idList) {
        for (int i = 0; i < idList.size(); i++) {
            UserInfo userInfo = userInfoDao.findById(idList.get(i)).get();
            userInfo.setHeadFlag(0);
            userInfoDao.save(userInfo);
        }
    }

    public void startUseStatus(List<Long> idList){
        for (int i = 0; i < idList.size(); i++) {
            UserInfo userInfo = userInfoDao.findById(idList.get(i)).get();
            userInfo.setUseStatus(0);
            userInfoDao.save(userInfo);
        }
    }

    public void stopUseStatus(List<Long> idList){
        for (int i = 0; i < idList.size(); i++) {
            UserInfo userInfo = userInfoDao.findById(idList.get(i)).get();
            userInfo.setUseStatus(1);
            userInfoDao.save(userInfo);
        }
    }

}
