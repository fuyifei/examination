package com.shinesend.miniapp.service;

import com.shinesend.base.service.SuperService;
import com.shinesend.miniapp.dao.ForumInfoDao;
import com.shinesend.miniapp.dto.ForumDTO;
import com.shinesend.miniapp.entity.ForumInfo;
import com.shinesend.web.util.QueryPageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ForumInfoService extends SuperService<ForumInfo, Long, ForumInfoDao> {

    @Autowired
    private ForumInfoDao forumInfoDao;

    public Page queryPage(ForumDTO forumDTO) {
        int currentNum = forumDTO.getPage().getCurrentNum();
        int pageSize = forumDTO.getPage().getPageSize();
        StringBuilder sqlSb = new StringBuilder();
        sqlSb.append(" select a.*,b.USER_NAME from Forum_Info a,USER_INFO b  where a.USER_ID = b.id and a.TOP_LEVEL =1 order by a.id desc ");
        sqlSb = QueryPageUtil.getQueryPageSql(sqlSb, pageSize, currentNum);
        List<Map<String, Object>> respList = forumInfoDao.queryMapBySql(sqlSb.toString());
        StringBuffer countSqlSb = new StringBuffer();
        countSqlSb.append(" select count(0) from Forum_Info where TOP_LEVEL =1 ");
        String objBySql = String.valueOf(forumInfoDao.getObjBySql(countSqlSb.toString()));
        PageRequest pageRequest = PageRequest.of(currentNum - 1, pageSize);
        return new PageImpl(respList, pageRequest, Long.parseLong(String.valueOf(objBySql)));
    }

    public List<Map<String, Object>> queryDtlById(Long parentId){
        StringBuilder sqlSb = new StringBuilder();
        sqlSb.append(" select a.*,b.USER_NAME from Forum_Info a,USER_INFO b  where a.USER_ID = b.id and a.PARENT_ID = "+parentId+" order by a.id desc ");
        return forumInfoDao.queryMapBySql(sqlSb.toString());
    }
    public List<ForumInfo> findAllByParentId(Long parentId ){
        return forumInfoDao.findAllByParentId(parentId);
    }

    public String queryDtlCount(Long parentId){
        StringBuilder countSqlSb = new StringBuilder();
        countSqlSb.append(" select count(0) from Forum_Info where PARENT_ID ="+parentId);
        return  String.valueOf(forumInfoDao.getObjBySql(countSqlSb.toString()));
    }

}
