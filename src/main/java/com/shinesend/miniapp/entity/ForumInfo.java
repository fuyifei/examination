package com.shinesend.miniapp.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.Date;

/**
 * 论坛表
 *
 * @author sqh
 **/
@Entity
@Table(name = "FORUM_INFO")
@Data
public class ForumInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_FORUM_INFO")
    @SequenceGenerator(name = "SEQ_FORUM_INFO", sequenceName = "SEQ_FORUM_INFO", allocationSize = 1)
    private Long id;

    @Column
    private Long userId;

    @Column
    private String content;
    @Column
    @CreationTimestamp
    private Date dataCreated;
    @Column
    @UpdateTimestamp
    private Date lastUpdated;
    @Column
    private Long parentId;
    @Column
    private Integer topLevel;
}
