package com.shinesend.miniapp.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;

/**
 * 用户表
 *
 * @author sqh
 **/
@Entity
@Table(name = "USER_INFO")
@Data
public class UserInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_USER_INFO")
    @SequenceGenerator(name = "SEQ_USER_INFO", sequenceName = "SEQ_USER_INFO", allocationSize = 1)
    private Long id;
    @Column
    private String userName;
    @Column
    private String wxOpenId;
    @Column
    private String mobile;
    @Column
    private String headSculptureUrl;

    @Column
    private Long imgId;
    @Column
    @CreationTimestamp
    private Date dataCreated;

    @Column
    @UpdateTimestamp
    private Date lastUpdated;

    @Transient
    private String sessionKey;

    @Transient
    private Long shopId;
    @Transient
    private String shopName;
    /**
     * grape用户ID
     */
    @Transient
    private Long grapeUserId;
    //企业微信 用户ID
    @Column
    private String qyUserId;

    /**
     * 店长标识
     */
    @Column
    private Integer headFlag;

    /**
     * 企业微信二维码
     */
    @Column
    private String qrImg;
    /**
     * 启用状态
     */
    @Column
    private Integer useStatus;

    @Column
    private Integer empowerFlag;
}
