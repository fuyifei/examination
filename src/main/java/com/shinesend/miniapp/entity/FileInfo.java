package com.shinesend.miniapp.entity;

import com.qiniu.util.IOUtils;
import com.qiniu.util.Md5;
import com.shinesend.miniapp.util.ConfigService;
import com.shinesend.miniapp.util.QiniuStorage;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PostRemove;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * 文件表
 *
 * @author sqh
 */
@Entity
@Table(name = "FILE_INFO")
@Data
@Slf4j
public class FileInfo {
    public FileInfo() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_FILE_INFO")
    @SequenceGenerator(name = "SEQ_FILE_INFO", sequenceName = "SEQ_FILE_INFO", allocationSize = 1)
    private Long id;
    @Column
    private String fileType;
    @Column
    private Double fileLength;
    @Column
    private String filePath;

    /**
     * 0 --> fileSystem
     * 1 --> qiniu
     */
    @Column
    private Integer storageType;

    @Column
    private String fileName;
    /**
     * 文件 Hash 值
     */
    @Column
    private String fileHash;

    @Column
    @CreationTimestamp
    private Date dataCreated;
    @Column
    @UpdateTimestamp
    private Date lastUpdated;

    @Transient
    private byte[] fileBytes;


    public FileInfo(String fileType, byte[] fileBytes) {
        this.fileType = fileType;
        this.fileBytes = fileBytes;
    }

    @PrePersist
    public void beforeSave() throws Exception {
        saveFile();
    }

    @PostRemove
    public void afterDelete() {
        deleteFile();
    }

    @PreUpdate
    public void beforeUpdate() throws Exception {
        String newFileHash = Md5.md5(fileBytes);
        if (!newFileHash.equals(fileHash)) {
            updateFile();
        }
    }

    private void saveFile() throws Exception {
        if (fileName == null || "".equals(fileName)) {
            fileName = UUID.randomUUID().toString().replaceAll("-", "");
        }
        fileHash = Md5.md5(fileBytes);
        ConfigService configService = new ConfigService();
        String fileStorageType = configService.getKey("file.storage.type", "filesystem");
        switch (fileStorageType) {
            case "filesystem":
                storageType = 0;
                String osPath = getOsPath();

                // yyyy-mm
                LocalDate now = LocalDate.now();
                String year = String.valueOf(now.getYear());
                String monthValue = String.valueOf(now.getMonthValue());
                filePath = String.format("%s-%s", year, monthValue);
                osPath = String.format("%s/%s", osPath, filePath);

                File storageDir = new File(osPath);
                if (!storageDir.exists()) {
                    storageDir.mkdirs();
                }

                FileOutputStream out = new FileOutputStream(String.format("%s/%s.%s", osPath, fileName, fileType));
                out.write(fileBytes);
                out.close();

                break;
            case "qiniu":
                storageType = 1;

                String accessKey = configService.getKey("file.storage.qiniu.accessKey", "S7XCy-qpPyzQ7uM9cQXPy9xAtF_wFGNO-ImHLxtS");
                String secretKey = configService.getKey("file.storage.qiniu.secretKey", "REu3wQcRKJtQjzIjJSxqL6CPAZhV1i0DJrdS7oAU");
                String bucketName = configService.getKey("file.storage.qiniu.bucketName", "sg-licence");
                QiniuStorage qiniuStorage = new QiniuStorage(accessKey, secretKey, bucketName);
                qiniuStorage.upload(fileBytes, String.format("%s.%s", fileName, fileType));
                break;
            default:
                throw new Exception("未定义的存储类型。");
        }
    }

    public void deleteFile() {
        ConfigService configService = new ConfigService();
        switch (storageType) {
            case 0:
                String osPath = getOsPath();
                osPath = String.format("%s/%s", osPath, filePath);
                File storageDir = new File(osPath);

                if (!storageDir.exists()) {
                    return;
                }

                // TODO 删除先这样写，如果要保存到系统文件，此代码在重写
                // 执行删除文件
                String fullFilePath = String.format("%s/%s.%s", osPath, fileName, fileType);
                File file = new File(fullFilePath);
                file.delete();
                break;

            case 1:
                String accessKey = configService.getKey("file.storage.qiniu.accessKey", "S7XCy-qpPyzQ7uM9cQXPy9xAtF_wFGNO-ImHLxtS");
                String secretKey = configService.getKey("file.storage.qiniu.secretKey", "REu3wQcRKJtQjzIjJSxqL6CPAZhV1i0DJrdS7oAU");
                String bucketName = configService.getKey("file.storage.qiniu.bucketName", "sg-licence");
                QiniuStorage qiniuStorage = new QiniuStorage(accessKey, secretKey, bucketName);
                qiniuStorage.delete(String.format("%s.%s", fileName, fileType));
                break;
            default:

        }
    }

    public void updateFile() throws Exception {
        saveFile();
        deleteFile();
    }

    public Map<String, Object> toMap() {
        Map<String, Object> en = new HashMap<>(4);
        en.put("fileName", fileName);
        en.put("fileType", fileType);
        en.put("fileLength", String.valueOf(fileLength));
        if (storageType == 0) {
            String osPath = getOsPath();
            String fullFilePath = String.format("%s/%s/%s.%s", osPath, filePath, fileName, fileType);
            try {
                en.put("fileBytes", Base64.encodeBase64String(IOUtils.toByteArray(new FileInputStream(fullFilePath))));
            } catch (IOException e) {
                log.error("文件未找到，地址[{}]，原因：{}", fullFilePath, e.getMessage(), e);
            }
        } else {
            en.put("fileUrl", getFileUrl());
        }

        return en;
    }

    /**
     * 取七牛云地址
     *
     * @return
     */
    public String getFileUrl() {
        ConfigService configService = new ConfigService();
        if (storageType != 1) {
            return null;
        }

        String fileUrl = "";
        try {
            String accessKey = configService.getKey("file.storage.qiniu.accessKey", "S7XCy-qpPyzQ7uM9cQXPy9xAtF_wFGNO-ImHLxtS");
            String secretKey = configService.getKey("file.storage.qiniu.secretKey", "REu3wQcRKJtQjzIjJSxqL6CPAZhV1i0DJrdS7oAU");
            String bucketName = configService.getKey("file.storage.qiniu.bucketName", "sg-licence");
            String domainOfBucket = configService.getKey("file.storage.qiniu.domainOfBucket", "http://sg-licence.shinesend.com");
            long expireInSeconds = Long.valueOf(configService.getKey("file.storage.qiniu.expireInSeconds", "3600"));
            QiniuStorage qiniuStorage = new QiniuStorage(accessKey, secretKey, bucketName);
            fileUrl = qiniuStorage.getDownloadUrl(String.format("%s.%s", fileName, fileType), domainOfBucket, expireInSeconds);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return fileUrl;
    }

    /**
     * 去本地图片 二进制
     */
    public byte[] getFileBytesForOs() {
        if (storageType == 0) {
            String osPath = getOsPath();
            String fullFilePath = String.format("%s/%s/%s.%s", osPath, filePath, fileName, fileType);
            try {
                return IOUtils.toByteArray(new FileInputStream(fullFilePath));
            } catch (IOException e) {
                log.error("文件未找到，地址[{}]，原因：{}", fullFilePath, e.getMessage(), e);
            }
        }
        return null;
    }

    /**
     * 获取系统文件路径
     *
     * @return
     */
    private String getOsPath() {
        ConfigService configService = new ConfigService();
        String dirName = configService.getKey("file.storage.filesystem.dir.name", ".grape");
        String osPath = configService.getKey("file.storage.filesystem.path", String.format("%s/%s", System.getProperty("user.home"), dirName));
        return osPath;
    }
}
