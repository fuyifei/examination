package com.shinesend.miniapp.dao;

import com.shinesend.base.repository.BaseRepository;
import com.shinesend.miniapp.entity.ForumInfo;

import java.util.List;

public interface ForumInfoDao extends BaseRepository<ForumInfo, Long> {

    List<ForumInfo> findAllByParentId(Long parentId);
}
