package com.shinesend.miniapp.dao;

import com.shinesend.base.repository.BaseRepository;
import com.shinesend.miniapp.entity.FileInfo;

public interface FileInfoDao extends BaseRepository<FileInfo, Long> {
}
