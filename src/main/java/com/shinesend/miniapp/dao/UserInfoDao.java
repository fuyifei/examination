package com.shinesend.miniapp.dao;

import com.shinesend.base.repository.BaseRepository;
import com.shinesend.miniapp.entity.UserInfo;

import java.util.List;

public interface UserInfoDao extends BaseRepository<UserInfo, Long> {

    List<UserInfo> findAllByWxOpenId(String wxOpenId);

}
