package com.shinesend.miniapp.dto;

import com.shinesend.base.service.query.Page;
import lombok.Data;

@Data
public class ForumDTO {
    private Long id;

    private Long userId;

    private String content;
    private Long parentId;
    private Integer topLevel;
    private Page page;
}
