package com.shinesend.miniapp.dto;

import com.shinesend.base.service.query.Page;
import lombok.Data;

@Data
public class QuestionDTO {
    private Long questionId;
    private Long sortId;
    private Integer result;
    private Page page;
}
