package com.shinesend.miniapp.dto;

import com.shinesend.base.service.query.Page;
import lombok.Data;

@Data
public class UserInfoDTO {
    private Long id;
    private String userName;
    private String wxOpenId;
    private String mobile;
    private String headSculptureUrl;
    private String code;
    private String encryptedData;
    private String iv;
    private String sessionKey;
    private String qwUserId;
    private Long shopId;
    private Integer headFlag;
    private String qrImg;
    private String erpName;
    private Integer useStatus;
    private Page page;

    private Long grapeUserId;
    private String grapeUserName;
}
