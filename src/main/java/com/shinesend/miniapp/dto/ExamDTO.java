package com.shinesend.miniapp.dto;

import lombok.Data;

@Data
public class ExamDTO {
    private Long id;
    private Double score;
}
