package com.shinesend.miniapp.controller;

import com.shinesend.base.result.ApiResult;
import com.shinesend.web.dto.mall.GoodsInfoDTO;
import com.shinesend.web.service.mall.GoodsInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/miniApp/goods")
public class GoodsController {
    @Autowired
    private GoodsInfoService goodsInfoService;
    @PostMapping("/queryGoods")
    public ApiResult queryGoods(@RequestBody GoodsInfoDTO goodsInfoDTO) {

        return ApiResult.success(goodsInfoService.queryPage(goodsInfoDTO));
    }
}
