package com.shinesend.miniapp.controller;

import com.shinesend.miniapp.entity.FileInfo;
import com.shinesend.miniapp.service.FileInfoService;
import com.shinesend.miniapp.util.FileDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author sqh
 **/
@RestController
@RequestMapping("/miniApp/fileInfo")
public class FileInfoController {
    @Autowired
    private FileInfoService fileInfoService;

    @PostMapping("/getImg")
    public String getImg(@RequestBody FileDTO fileDTO) {

        FileInfo fileInfoSel = fileInfoService.get(fileDTO.getId());
        if (fileInfoSel != null) {
            Map<String, Object> stringObjectMap = fileInfoSel.toMap();
            return (String) stringObjectMap.get("fileBytes");
        }
        return "";
    }

    @GetMapping("/getImage/{id}")
    public String getPostImg(@PathVariable Long id) {
        FileInfo fileInfoSel = fileInfoService.get(id);
        if (fileInfoSel != null) {
            Map<String, Object> stringObjectMap = fileInfoSel.toMap();
            return (String) stringObjectMap.get("fileBytes");
        }
        return "";
    }
}
