package com.shinesend.miniapp.controller;

import com.shinesend.base.result.ApiResult;
import com.shinesend.base.util.JwtUtil;
import com.shinesend.miniapp.dto.UserInfoDTO;
import com.shinesend.miniapp.entity.UserInfo;
import com.shinesend.miniapp.service.FileInfoService;
import com.shinesend.miniapp.service.UserInfoService;
import com.shinesend.miniapp.util.MobileDTO;
import com.shinesend.web.util.QiYeWeiXinUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sqh
 **/
@RestController
@RequestMapping("/miniApp/userInfo")
public class UserInfoController {
    @Autowired
    private UserInfoService userInfoService;
    @Autowired
    private FileInfoService fileInfoService;
    @Autowired
    private QiYeWeiXinUtil qiYeWeiXinUtil;

    /**
     * 小程序登录
     *
     * @param userInfoDTO
     * @return
     */
    @PostMapping("/login")
    public ApiResult login(@RequestBody UserInfoDTO userInfoDTO) {
        UserInfo userInfo = userInfoService.login(userInfoDTO);
        if(userInfo.getUseStatus()==1){
            return ApiResult.failure("您已被停用请联系管理员");
        }
        String token = JwtUtil.genSysToken(Integer.valueOf(String.valueOf(userInfo.getId())), userInfo.getWxOpenId());
        Map<String, Object> respData = new HashMap<>(2);
        respData.put("userInfo", userInfo);
        respData.put("token", token);
        return ApiResult.success(respData);
    }

    @PostMapping("/qyLogin")
    public ApiResult qyLogin(@RequestBody UserInfoDTO userInfoDTO) {
        String code = userInfoDTO.getCode();
        return ApiResult.success(qiYeWeiXinUtil.qyLogin(code));
    }

    /**
     * 修改小程序用户信息
     *
     * @param userInfoDTO
     * @return
     */
    @PostMapping("/update")
    public ApiResult update(@RequestBody UserInfoDTO userInfoDTO) {
        Long userId = userInfoDTO.getId();
        String qwUserId = userInfoDTO.getQwUserId();
        String qrImg = userInfoDTO.getQrImg();
        UserInfo userInfo = userInfoService.get(userId);
        userInfo.setUserName(userInfoDTO.getUserName());
        if (StringUtils.isNotBlank(qwUserId)) {
            userInfo.setQyUserId(qwUserId);
        }
        String mobile = userInfoDTO.getMobile();
        userInfo.setMobile(mobile);
        if (userInfoDTO.getHeadSculptureUrl() != null) {
            if (userInfo.getHeadSculptureUrl() == null || !userInfo.getHeadSculptureUrl().equals(userInfoDTO.getHeadSculptureUrl())) {
                if (userInfo.getImgId() != null) {
                    fileInfoService.delete(userInfo.getImgId());
                }

                userInfo = userInfoService.savePicture(userInfoDTO.getHeadSculptureUrl(), userInfo);
                userInfo.setHeadSculptureUrl(userInfoDTO.getHeadSculptureUrl());
            }

        }
        if (StringUtils.isNotBlank(qrImg)) {
            userInfo.setQrImg(qrImg);
        }

        userInfo = userInfoService.update(userInfo);
        return ApiResult.success(userInfo);
    }

    /**
     * 根据手机号 获取 grape会员
     *
     * @param userInfoDTO
     * @return
     */
    @PostMapping("/queryGrapeUserByMobile")
    public ApiResult queryGrapeUserByMobile(@RequestBody UserInfoDTO userInfoDTO) {
        String mobile = userInfoDTO.getMobile();
        if (StringUtils.isNotBlank(mobile)) {
            return ApiResult.success(userInfoService.queryGrapeUserByMobile(mobile));
        } else {
            return ApiResult.failure("手机号不能为空");
        }
    }





    @PostMapping("/getMobile")
    public ApiResult getMobile(@RequestBody MobileDTO mobileDTO) {

        return ApiResult.success(userInfoService.decryptWxUserPhoneNumber(mobileDTO));
    }
    @PostMapping("/saveMobile")
    public ApiResult saveMobile(@RequestBody UserInfoDTO userInfoDTO){
        Long id = userInfoDTO.getId();
        String mobile = userInfoDTO.getMobile();
        UserInfo userInfo = userInfoService.get(id);
        userInfo.setMobile(mobile);
        userInfoService.update(userInfo);
       return ApiResult.success();
    }
    @PostMapping("/setHeadFlag")
    public ApiResult setHeadFlag(@RequestBody UserInfoDTO userInfoDTO) {
        if (userInfoDTO.getHeadFlag() == 1) {
            List<Long> idList = new ArrayList<>();
            idList.add(userInfoDTO.getId());
            userInfoService.setShopHead(idList);
        }

        return ApiResult.success();
    }

    @PostMapping({"/setEmpowerFlag"})
    public ApiResult setEmpowerFlag(@RequestBody UserInfoDTO userInfoDTO) { Long userId = userInfoDTO.getId();
        UserInfo userInfo = userInfoService.get(userId);
        userInfo.setEmpowerFlag(Integer.valueOf(1));
        userInfoService.update(userInfo);
        return ApiResult.success();
    }
}
