package com.shinesend.miniapp.controller;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.shinesend.base.result.ApiResult;
import com.shinesend.base.util.JwtUtil;
import com.shinesend.miniapp.dto.ForumDTO;
import com.shinesend.miniapp.entity.ForumInfo;
import com.shinesend.miniapp.service.ForumInfoService;
import com.shinesend.web.dto.IdsDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/miniApp/forum")
public class ForumInfoController {
    @Autowired
    private ForumInfoService forumInfoService;

    @PostMapping("/queryForum")
    public ApiResult queryForum(@RequestBody ForumDTO forumDTO) {
        return ApiResult.success(forumInfoService.queryPage(forumDTO));
    }

    @PostMapping("/queryForumByPId")
    public ApiResult queryForumByPId(@RequestBody ForumDTO forumDTO) {
        return ApiResult.success(forumInfoService.queryDtlById(forumDTO.getParentId()));
    }

    @PostMapping("/addForum")
    public ApiResult addForum(@RequestBody ForumDTO forumDTO, @RequestHeader("token") String token) {
        DecodedJWT verify = JwtUtil.verifySys(token);
        Long userId = verify.getClaim("userId").asLong();
        ForumInfo forum = new ForumInfo();
        Long parentId = forumDTO.getParentId();
        forum.setParentId(parentId);
        forum.setContent(forumDTO.getContent());
        if (parentId == null) {
            forum.setTopLevel(1);
        } else {
            forum.setTopLevel(0);
        }

        forum.setUserId(userId);
        forumInfoService.insert(forum);
        return ApiResult.success();
    }

    @PostMapping("/deleteForum")
    public ApiResult deleteForum(@RequestBody IdsDTO idsDTO) {
        List<Long> idList = idsDTO.getIds();
        List<Long> deleteIdList = new ArrayList<>();
        for (int i = 0; i < idList.size(); i++) {
            List<ForumInfo> forumList = new ArrayList<>();
            forumList = forumInfoService.findAllByParentId(idList.get(i));
                for (int j = 0; j < forumList.size(); j++) {
                    forumList = forumInfoService.findAllByParentId(forumList.get(j).getId());
                    deleteIdList.add(forumList.get(j).getId());
                }
            deleteIdList.add(idList.get(i));
        }
        for (int i = 0; i < deleteIdList.size(); i++) {
            forumInfoService.delete(deleteIdList.get(i));
        }
        return ApiResult.success();
    }
    @PostMapping("/queryDtlCount")
    public ApiResult queryDtlCount(@RequestBody ForumDTO forumDTO){

        return ApiResult.success(forumInfoService.queryDtlCount(forumDTO.getParentId()));
    }
}
