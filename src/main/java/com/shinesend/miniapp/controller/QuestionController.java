package com.shinesend.miniapp.controller;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.shinesend.base.result.ApiResult;
import com.shinesend.base.util.JwtUtil;
import com.shinesend.miniapp.dto.ExamDTO;
import com.shinesend.miniapp.dto.QuestionDTO;
import com.shinesend.web.entity.question.ErrorLog;
import com.shinesend.web.entity.question.ExamLog;
import com.shinesend.web.service.question.AnswerService;
import com.shinesend.web.service.question.ErrorLogService;
import com.shinesend.web.service.question.ExamLogService;
import com.shinesend.web.service.question.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/miniApp/question")
public class QuestionController {

    @Autowired
    private QuestionService questionService;
    @Autowired
    private AnswerService answerService;
    @Autowired
    private ExamLogService examLogService;
    @Autowired
    private ErrorLogService errorLogService;

    @PostMapping("/queryQuestionBySortId")
    public ApiResult queryQuestionBySortId(@RequestBody QuestionDTO questionDTO, @RequestHeader("token") String token) {
        DecodedJWT verify = JwtUtil.verifySys(token);
        Long userId = verify.getClaim("userId").asLong();
        return ApiResult.success(questionService.findOneBySortId(questionDTO.getSortId(), userId));
    }

    @PostMapping("/queryAnswerByQuestionId")
    public ApiResult queryAnswerByQuestionId(@RequestBody QuestionDTO questionDTO) {

        return ApiResult.success(answerService.findAllByQuestionId(questionDTO.getQuestionId()));
    }

    @PostMapping("/getOneExamQuestions")
    public ApiResult getOneExamQuestions(@RequestBody QuestionDTO questionDTO, @RequestHeader("token") String token) {
        DecodedJWT verify = JwtUtil.verifySys(token);
        Long userId = verify.getClaim("userId").asLong();
        Long sortId = questionDTO.getSortId();
        ExamLog examLog = new ExamLog();
        examLog.setUserId(userId);
        examLog.setSortId(sortId);
        examLog = examLogService.insert(examLog);
        Map<String, Object> result = new HashMap<>();
        result.put("exam", examLog);
        result.put("questionList", questionService.findAllBySortId(sortId));
        return ApiResult.success(result);
    }

    @PostMapping("/saveErrorLog")
    public ApiResult saveErrorLog(@RequestBody QuestionDTO questionDTO, @RequestHeader("token") String token) {
        DecodedJWT verify = JwtUtil.verifySys(token);
        Long userId = verify.getClaim("userId").asLong();
        Long questionId = questionDTO.getQuestionId();
        Integer result = questionDTO.getResult();
        ErrorLog log = new ErrorLog();
        log.setQuestionId(questionId);
        log.setResultStatus(result);
        log.setUserId(userId);
        errorLogService.insert(log);
        return ApiResult.success();
    }

    @PostMapping("/queryErrorLog")
    public ApiResult queryErrorLog(@RequestBody QuestionDTO questionDTO, @RequestHeader("token") String token) {
        DecodedJWT verify = JwtUtil.verifySys(token);
        Long userId = verify.getClaim("userId").asLong();
        Long sortId = questionDTO.getSortId();
        return ApiResult.success(questionService.findOneErrorBySortId(sortId, userId));
    }
    @PostMapping("/overExam")
    public ApiResult overExam(@RequestBody ExamDTO examDTO){
        ExamLog exam = examLogService.get(examDTO.getId());
        exam.setScore(examDTO.getScore());
        examLogService.update(exam);
        return ApiResult.success();
    }

}
