package com.shinesend.miniapp.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import java.io.IOException;
import java.util.Properties;

/**
 * 读取存储相关配置
 *
 * @author sqh
 **/
@Slf4j
public class ConfigService {

    private static final String CONFIG_NAME = "config/config.properties";

    public static String getKey(String key, String defaultValue) {
        Properties properties;
        try {
            properties = PropertiesLoaderUtils.loadAllProperties(CONFIG_NAME);
        } catch (IOException e) {
            log.error("读取 [{}] 配置文件错误，返回默认结果：{}，原因：{}", CONFIG_NAME, defaultValue, e.getMessage(), e);
            return defaultValue;
        }
        return properties.getProperty(key, defaultValue);
    }

    public static String getKey(String key) {
        return getKey(key, null);
    }
}
