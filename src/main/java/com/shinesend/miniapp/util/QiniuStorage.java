package com.shinesend.miniapp.util;

import com.qiniu.common.QiniuException;
import com.qiniu.http.Response;
import com.qiniu.storage.BucketManager;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.Region;
import com.qiniu.storage.UploadManager;
import com.qiniu.util.Auth;
import lombok.extern.slf4j.Slf4j;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * 七牛云文件上传与下载
 *
 * @author sqh
 **/
@Slf4j
public class QiniuStorage {
    private String accessKey;
    private String secretKey;
    private String bucketName;

    private QiniuStorage() {
    }

    public QiniuStorage(String accessKey, String secretKey, String bucketName) {
        this.accessKey = accessKey;
        this.secretKey = secretKey;
        this.bucketName = bucketName;
    }


    /**
     * 文件上传
     *
     * @param fileBytes 文件字节数组
     * @return 上传结果
     * @throws QiniuException 异常
     */
    public String upload(byte[] fileBytes) throws QiniuException {
        return upload(fileBytes, null);
    }

    /**
     * 文件上传
     *
     * @param fileBytes 文件字节数组
     * @return 上传结果
     * @throws QiniuException 异常
     */
    public String upload(byte[] fileBytes, String fileName) throws QiniuException {
        Configuration cfg = new Configuration(Region.huabei());
        UploadManager uploadManager = new UploadManager(cfg);

        Auth auth = Auth.create(accessKey, secretKey);
        Response response = uploadManager.put(fileBytes, fileName, auth.uploadToken(bucketName));

        if (!response.isOK()) {
            throw new QiniuException(response);
        }
        return response.bodyString();
    }

    /**
     * 获取下载链接
     *
     * @param fileName        文件名称
     * @param domainOfBucket  域名
     * @param expireInSeconds 过期时间
     * @return 路径
     * @throws UnsupportedEncodingException 异常
     */
    public String getDownloadUrl(String fileName, String domainOfBucket, long expireInSeconds) throws UnsupportedEncodingException {
        String encodedFileName = URLEncoder.encode(fileName, "utf-8").replace("+", "20%");
        String publicUrl = String.format("%s/%s", domainOfBucket, encodedFileName);

        Auth auth = Auth.create(accessKey, secretKey);
        return auth.privateDownloadUrl(publicUrl, expireInSeconds);
    }

    /**
     * 删除文件
     *
     * @param key 文件名称
     */
    public void delete(String key) {
        Configuration cfg = new Configuration(Region.huabei());
        Auth auth = Auth.create(accessKey, secretKey);
        BucketManager bucketManager = new BucketManager(auth, cfg);
        try {
            bucketManager.delete(bucketName, key);
        } catch (QiniuException ex) {
            log.error("七牛云文件删除失败，bucket 名称：{}，文件名称{}，{}", bucketName, key, ex.response.toString(), ex);
        }
    }
}
