package com.shinesend.miniapp.util;

import lombok.Data;

@Data
public class MobileDTO {
    private String encryptedData;
    private String iv;
    private String sessionKey;
    private Long userId;
}
